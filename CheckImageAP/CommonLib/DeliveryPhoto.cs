﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CommonLib
{
    public class DeliveryPhoto
    {
        public static string strConnMain = "";
		public static string strConnLog = "";

		public enum CONN_TYPE
        {
            MAIN = 0 , 
			LOG = 1
        }
        public static void setConn(string strConnType , string strConn)
        {
            if (strConnType.Equals(CONN_TYPE.MAIN.ToString()))
            {
                strConnMain = strConn;
            }
			else if (strConnType.Equals(CONN_TYPE.LOG.ToString()))
			{
				strConnLog = strConn;
			}
		}

        public static void InsertDeliveryNew()
        {
            DateTime now = DateTime.Now;


            try
            {
                using (var cn = new SqlConnection(strConnMain))
                {
                    string strSQL = @"
Declare @yyyymmdd datetime 
Set @yyyymmdd = convert( nvarchar(8) , dateadd( day , 0 , getdate() ) , 112 )

Insert Into [CheckDeliveryPhotoData] ([request_id],check_number,yyyymmdd,is_delete)
	Select	[request_id] , 
			check_number , 
			convert( nvarchar(8) , cdate , 112 ) as yyyymmdd, 
			case 
				When cancel_date is null or cancel_date = '' then 0
				else 1 
			End is_delete 		
	From tcDeliveryRequests with (nolock)
	Where request_id > ( Select max(request_id) From CheckDeliveryPhotoData with (nolock))
	and cdate < @yyyymmdd
	Order by [request_id]

Set @yyyymmdd = convert( nvarchar(8) , dateadd( day , -90 , getdate() ) , 112 )

Update CheckDeliveryPhotoData
	Set check_number = a.check_number
From tcDeliveryRequests a
Where a.cdate >= @yyyymmdd
and CheckDeliveryPhotoData.yyyymmdd >= @yyyymmdd
and len(a.check_number) > 5
--and a.check_number = CheckDeliveryPhotoData.check_number
and ( len( CheckDeliveryPhotoData.check_number) < 6 or CheckDeliveryPhotoData.check_number is null )
and CheckDeliveryPhotoData.request_id = a.request_id
";
                    cn.Execute(strSQL);
                }
            }
            catch (Exception e)
            {

            }



            /*
            Insert Into [dbo].[CheckDeliveryPhotoData] 
                    (request_id , check_number , yyyymmdd , is_delete)
            Select	request_id , 
                    check_number , 
                    format(cdate, 'yyyyMMdd') as yyyymmdd , 
                    Case	
                        When cancel_date is null or cancel_date = '' then 0
                        else 1 
                    End as is_delete
            From tcDeliveryRequests with (nolock)
            Where cdate >= '20200101'
            and cdate <= '20220601'
            and ( cancel_date is null or cancel_date = '' )
            and len(check_number) > 0
            Order by request_id


            Select * From [CheckDeliveryPhotoData]
            */
        }

        public static List<CheckDeliveryPhotoData> GetDeliveryRecord_Hour()
        {
            DateTime now = DateTime.Now;
            string StartYYYYMMDD = "";

            StartYYYYMMDD = now.AddDays(-245).ToString("yyyyMMdd");

			List<CheckDeliveryPhotoData> returnData = new List<CheckDeliveryPhotoData>();

			try
			{
				using (var cn = new SqlConnection(strConnMain))
				{
					string strSQL = @"
/*
Declare @StartYYYYMMDD nvarchar(8)
Set @StartYYYYMMDD = '20220101'
*/
Select  a.request_id ,
		b.[check_number],
		a.[yyyymmdd],
		a.is_delete2,
		isnull(b.cancel_date,0) as [is_delete] ,
		b.latest_scan_item , 
		a.[photo_paper],
		a.[is_photo_paper_ok] , 
		a.[photo_digi_1],
		a.[is_photo_digi_1_ok] , 
		a.[photo_digi_2],
		a.[is_photo_digi_2_ok] , 
		a.[udate] , 
		c.sign_form_image , 
		c.sign_field_image , 
		c.ImageTime
From 
(
	SELECT	request_id ,
			[check_number],
			[yyyymmdd],
			--[is_delete],
			Case 
				When [is_delete] = 0 then '0'
				When [is_delete] = 1 then '1'
				else '0'
			End as is_delete2,
			[photo_paper],
			[is_photo_paper_ok] , 
			[photo_digi_1],
			[is_photo_digi_1_ok] , 
			[photo_digi_2],
			[is_photo_digi_2_ok] , 
			[udate]
	FROM [dbo].[CheckDeliveryPhotoData] with (nolock)
	Where [yyyymmdd] >= @StartYYYYMMDD 
) a
left join 
(
	Select	request_id ,
			check_number , 
			latest_scan_item ,
			case 
				When cancel_date is null or cancel_date = '' then 0
				else 1 
			End cancel_date 
	From tcDeliveryRequests with (nolock)
	Where cdate >= @StartYYYYMMDD 
) b on a.request_id = b.request_id 
inner join 
(
	Select cdate as ImageTime , check_number , sign_form_image , sign_field_image From ttDeliveryScanLog with (nolock)
	Where cdate >= @StartYYYYMMDD
	and scan_item in ('4')
	and log_id in (
					Select  max(log_id) as log_id From ttDeliveryScanLog with (nolock)
					Where cdate >= @StartYYYYMMDD 
					and scan_item in ('4')
					and check_number in (
										Select  check_number From ttDeliveryScanLog with (nolock)
										Where cdate >= dateadd( hour , -2 , getdate() )
										and scan_item in ('4')
										)
					and sign_field_image <> '' 
					and sign_form_image <> ''
					Group by check_number
				)
) c on a.check_number = c.check_number
Order by a.request_id

";
					returnData = (List<CheckDeliveryPhotoData>)cn.Query<CheckDeliveryPhotoData>(strSQL,
						new
						{
							StartYYYYMMDD = StartYYYYMMDD
						}
						);
				}
			}
			catch (Exception e)
			{

			}

			return returnData;
		}

        public static List<CheckDeliveryPhotoData> GetDeliveryRecord(string StartYYYYMMDD = "" , string EndYYYYMMDD_1 = "" , string EndYYYYMMDD_2 = "")
        {
            DateTime now = DateTime.Now;

            if ( StartYYYYMMDD.Equals("") )
            {
                StartYYYYMMDD = now.AddDays(-245).ToString("yyyyMMdd");
                EndYYYYMMDD_1 = now.ToString("yyyyMMdd") ;
                EndYYYYMMDD_2 = now.ToString("yyyyMMdd");
            }
            else
            {
                //now = new DateTime(int.Parse(StartYYYYMMDD.Substring(0, 4)),
                //                    int.Parse(StartYYYYMMDD.Substring(4, 2)),
                //                    int.Parse(StartYYYYMMDD.Substring(6, 2)));

                //EndYYYYMMDD_1 = now.AddDays(90).ToString("yyyyMMdd");
                //EndYYYYMMDD_2 = now.AddDays(120).ToString("yyyyMMdd");
            }

            List<CheckDeliveryPhotoData> returnData = new List<CheckDeliveryPhotoData>();

            try
            {
                using (var cn = new SqlConnection(strConnMain))
                {
                    string strSQL = @"
/*
Declare @StartYYYYMMDD nvarchar(8)
Declare @EndYYYYMMDD_1 nvarchar(8)
Declare @EndYYYYMMDD_2 nvarchar(8)

Set @StartYYYYMMDD = '20220101'
Set @EndYYYYMMDD_1 = '20220401'
Set @EndYYYYMMDD_2 = '20220501'
*/

Select  a.request_id ,
		b.[check_number],
		a.[yyyymmdd],
		a.is_delete2,
		isnull(b.cancel_date,0) as [is_delete] ,
		b.latest_scan_item , 
		a.[photo_paper],
		a.[is_photo_paper_ok] , 
		a.[photo_digi_1],
		a.[is_photo_digi_1_ok] , 
		a.[photo_digi_2],
		a.[is_photo_digi_2_ok] , 
		a.[udate] , 
		c.sign_form_image , 
		c.sign_field_image , 
		c.ImageTime
From 
(
	SELECT	request_id ,
			[check_number],
			[yyyymmdd],
			--[is_delete],
			Case 
				When [is_delete] = 0 then '0'
				When [is_delete] = 1 then '1'
				else '0'
			End as is_delete2,
			[photo_paper],
			[is_photo_paper_ok] , 
			[photo_digi_1],
			[is_photo_digi_1_ok] , 
			[photo_digi_2],
			[is_photo_digi_2_ok] , 
			[udate]
	FROM [dbo].[CheckDeliveryPhotoData] with (nolock)
	Where [yyyymmdd] between @StartYYYYMMDD and @EndYYYYMMDD_1 
	--and is_delete = 0 
	--and 
	--(
	--	(
	--	is_photo_paper_ok <> 1
	--	or is_photo_digi_1_ok <> 1
	--	or is_photo_digi_2_ok <> 1
	--	)
	--	or 
	--	(
	--	is_photo_paper_ok is null
	--	or is_photo_digi_1_ok is null
	--	or is_photo_digi_2_ok is null
	--	)
	--)
) a
left join 
(
	Select	request_id ,
			check_number , 
			latest_scan_item ,
			case 
				When cancel_date is null or cancel_date = '' then 0
				else 1 
			End cancel_date 
	From tcDeliveryRequests with (nolock)
	Where cdate between @StartYYYYMMDD and DateAdd(day , 1 , @EndYYYYMMDD_1 )
) b on a.request_id = b.request_id 
left join 
(
	Select cdate as ImageTime , check_number , sign_form_image , sign_field_image From ttDeliveryScanLog with (nolock)
	Where cdate >= @StartYYYYMMDD and cdate <= @EndYYYYMMDD_2
	and scan_item in ('4')
	and log_id in (
					Select  max(log_id) as log_id From ttDeliveryScanLog with (nolock)
					Where cdate >= @StartYYYYMMDD and cdate <= @EndYYYYMMDD_2
					and scan_item in ('4')
					and check_number in (
									Select check_number
									From tcDeliveryRequests with (nolock)
									Where cdate between @StartYYYYMMDD and DateAdd(day , 1 , @EndYYYYMMDD_1 )
										)
					and sign_field_image <> '' 
					and sign_form_image <> ''
					Group by check_number
				)
) c on a.check_number = c.check_number
--Where isnull(b.cancel_date,0) = 1
Order by a.request_id



";
                    returnData = (List<CheckDeliveryPhotoData>)cn.Query<CheckDeliveryPhotoData>(strSQL,
                        new
                        {
                            StartYYYYMMDD = StartYYYYMMDD,
                            EndYYYYMMDD_1 = EndYYYYMMDD_1,
                            EndYYYYMMDD_2 = EndYYYYMMDD_2
                        }
                        );
                }
            }
            catch (Exception e)
            {

            }

            return returnData;

        }

        public static int UpdateRecord(List<CheckDeliveryPhotoData> todoCheckList , DateTime CheckTime)
        {
			var query = from single in todoCheckList
						where single.udate > CheckTime
						select single;

			try
			{
				using (var cn = new SqlConnection(strConnMain))
				{
					string strSQL = @"
Update CheckDeliveryPhotoData
    Set is_delete = @is_delete , 
        photo_paper = @photo_paper , 
        photo_digi_1 = @photo_digi_1 , 
        photo_digi_2 = @photo_digi_2 , 
        is_photo_paper_ok = @is_photo_paper_ok , 
        is_photo_digi_1_ok = @is_photo_digi_1_ok , 
        is_photo_digi_2_ok = @is_photo_digi_2_ok , 
        udate = @udate
Where 1=1
and yyyymmdd = @yyyymmdd
and request_id = @request_id
and check_number = @check_number

";
					cn.Execute(strSQL, query);
				}
			}
			catch (Exception e)
			{

			}

			return query.Count();
		}

		public static void InsertRecord(JobRecord tobj)
		{
			try
			{
				using (var cn = new SqlConnection(strConnLog))
				{


					string strSQL = @"
INSERT INTO [dbo].[JobRecord]
           ([Type],[SubType],[Action],[Subject],[ContentBody],[LinkRelationData],[IsDoNotification],[MEMO])
     VALUES
           (@Type , @SubType , @Action , @Subject , @ContentBody , @LinkRelationData , @IsDoNotification , @MEMO  ) ";
					cn.ExecuteScalar(strSQL, new
					{
						Type = tobj.Type,
						SubType = tobj.SubType,
						Action = tobj.Action,
						Subject = tobj.Subject,
						ContentBody = tobj.ContentBody,
						LinkRelationData = tobj.LinkRelationData,
						IsDoNotification = tobj.IsDoNotification,
						MEMO = tobj.MEMO
					});
				}
			}
			catch (Exception e)
			{

			}
		}

	}


    public class CheckDeliveryPhotoData
    {
        public long request_id { get; set; }
        public string check_number { get; set; }
        public string yyyymmdd { get; set; }
        public string is_delete { get; set; }
		public string is_delete2 { get; set; }
		public string latest_scan_item { get; set; }
        public string photo_paper { get; set; }
        public bool? is_photo_paper_ok { get; set; }
        public string photo_digi_1 { get; set; }
        public bool? is_photo_digi_1_ok { get; set; }
        public string photo_digi_2 { get; set; }
        public bool? is_photo_digi_2_ok { get; set; }
        public DateTime? udate { get; set; }
        public string sign_form_image { get; set; }
        public string sign_field_image { get; set; }

		public DateTime ImageTime { get; set; }

	}


	public class JobRecord
	{
		public JobRecord(string strType, string strSubType, string strAction, string strSubject, string strContentBody,
						   string strLinkRelationData = "", int iIsDoNotification = 0, string strMEMO = "")
		{
			Type = strType;
			SubType = strSubType;
			Action = strAction;
			Subject = strSubject;
			ContentBody = strContentBody;
			LinkRelationData = strLinkRelationData;
			IsDoNotification = iIsDoNotification;
			MEMO = strMEMO;
		}
		public string Type = "";
		public string SubType = "";
		public string Action = "";
		public string Subject = "";
		public string ContentBody = "";
		public string LinkRelationData = "";
		public int IsDoNotification = 0;
		public string MEMO = "";
	}


}
