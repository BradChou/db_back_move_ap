﻿using CommonLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveDeliveryPhoto
{
    public partial class Form1 : Form
    {

        /*

        簽單排程搬家AP備註  2022.07.27

        **排程時段

        查找 ScanLog 最近 兩小時的簽單紀錄
        1. 目前白天時段 每05分鐘執行一次 ( 08:13 ~ 22:13 ) 
        2. 目前離峰時間 每10分鐘執行一次 ( 22:25 ~ 23:45 )
        
        查找 最近 240天 的簽單紀錄
        1. 早上 07:03 中午 12:03 傍晚 18:03 ( 原意是要處理 紙本簽單掃描的排程 , 但目前已沒有紙本簽單 )
        2. 半夜 00:03 ( 附加處理 .33 Web Sevice 過期超過35天的簽單 搬家到待刪除區 ) 

        **執行邏輯
        
        1. 拉取指定時間的簽單紀錄
        2. 簽單搬家
            2.1. 從 .33 Web Sevice 複製圖片到 .45 ( 一般電子簽收/拍照簽收 )
            2.2. 從 .45 -> .45 ( 紙本掃描 / 蝴蝶補簽單 )
        3. 更新 Table CheckDeliveryPhotoData 的紀錄
        4. ( 半夜 00:03 排程) 過期簽單 搬家到待刪除區 
            4.1. 必須同時符合 主表開單日超過35天 + 簽單產生日超過35天 
            4.2. .33 Web Sevice / .45 紙本簽單上傳 / 蝴蝶補簽單  的簽單暫存區


        **相關路徑
        
        簽單新家路徑 ( 依主表 cdate 的日期存入子資料夾 )
        \\172.30.1.45\SignPaperPhoto\Photo
        
        紙本簽單 / 蝴蝶補簽單的暫存路徑
        \\172.30.1.45\SignPaperPhoto\_No_Partition_Photo

        .33 Web Sevice 過期超過35天的簽單 搬家到待刪除區 ( 等候移除 ) 
        \\172.30.1.45\SignPaperPhoto\zzDelete

        .33 Web Sevice 的電子簽單存放區 
        \\172.30.1.33\d$\work\webservice\PHOTO

        **一些離奇的事情
        
        1. 蝴蝶系統的補上傳簽單 , 目前可以任意補上簽單 , 因此 , 需額外判斷 , 目前手上的電子簽單 , 是不是等於最新簽單
        2. 偶爾會發生 ScanLog 的 trigger 失效 , 導致主表沒有更新 latest_scan_item , 因此判斷方式有特化修改
        3. 為了擔心在 待刪除區 的簽單 , 有任何可能想再度利用 , 所以暫時不真的刪除
        
        */
        public Form1()
        {
            InitializeComponent();
        }

        private string StartYYYYMMDD = "";
        private string EndYYYYMMDD_1 = "";
        private string EndYYYYMMDD_2 = "";

        enum ENUM_COPY_PHOTO_TYPE
        {
            WEB_SERVICE_TO_45VM = 0,
            _35VM_TO_45VM = 1 ,
            _45VM_No_Partition_TO_45VM = 2
        }

        private DateTime TimeStart = DateTime.Now;

        private List<CheckDeliveryPhotoData> todoCheckList = new List<CheckDeliveryPhotoData>();
        private void Form1_Load(object sender, EventArgs e)
        {
            DeliveryPhoto.setConn(DeliveryPhoto.CONN_TYPE.MAIN.ToString(),
                                    System.Configuration.ConfigurationManager.AppSettings["connstr"].ToString());

            DeliveryPhoto.setConn(DeliveryPhoto.CONN_TYPE.LOG.ToString(),
                        System.Configuration.ConfigurationManager.AppSettings["connstr_log"].ToString());
        }

        int iStartKey = 0;

        //累積更新筆數
        int iTotalCount = 0;
        int iUpdateCount = 0;
        int iMoveCount = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            iStartKey += 1;
            label1.Text = "訊息 : 倒數 " + (20 - iStartKey).ToString() + " 秒 , 排程即將自動開始";

            if (iStartKey >= 20)
            {
                button1.Enabled = false;
                label1.Text = "";
                timer1.Enabled = false;
                Main();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            label1.Text = "";
            timer1.Enabled = false;
            Main();
        }

        private bool isHour = true;

        private void Main()
        {
            if (TimeStart.Hour < 8 
                    || (TimeStart.Hour == 12 && TimeStart.Minute < 5)
                    || (TimeStart.Hour == 18 && TimeStart.Minute < 5)
                    )
            {
                isHour = false;
            }
            else
            {
                isHour = true;
            }

            //isHour = false;
            //Main_Total(true);
            if (isHour == true)
            {
                Main_Hour();
            }
            if (isHour == false)
            {
                if (TimeStart.Hour < 2)
                {
                    Main_Total(true);
                }
                else
                {
                    Main_Total(false);
                }
            }

        }

        private void Main_Hour()
        {
            DateTime checkStart = TimeStart.AddDays(-220);
            DateTime checkEnd = TimeStart;

            if (txtStart.Text != "")
            {
                int a = int.Parse(txtStart.Text.Substring(0, 4));
                int b = int.Parse(txtStart.Text.Substring(4, 2));
                int c = int.Parse(txtStart.Text.Substring(6, 2));
                checkStart = new DateTime(a, b, c);
            }
            if (txtEnd.Text != "")
            {
                int a = int.Parse(txtEnd.Text.Substring(0, 4));
                int b = int.Parse(txtEnd.Text.Substring(4, 2));
                int c = int.Parse(txtEnd.Text.Substring(6, 2));
                checkEnd = new DateTime(a, b, c);
            }

            txtBox.AppendText("準備開始 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");
            txtBox.AppendText("範圍 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkEnd.ToString("yyyy-MM-dd") + "  \r\n \r\n ");
            label2.Text = "範圍 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkEnd.ToString("yyyy-MM-dd");
            Application.DoEvents();

            try
            {
                StartYYYYMMDD = checkStart.ToString("yyyyMMdd");
                EndYYYYMMDD_1 = checkEnd.ToString("yyyyMMdd");
                EndYYYYMMDD_2 = checkEnd.ToString("yyyyMMdd");

                txtBox.AppendText("處理日期 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkEnd.ToString("yyyy-MM-dd") + "  \r\n ");
                txtBox.AppendText("現在時刻 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
                Application.DoEvents();

                if (todoCheckList.Count > 0)
                {
                    todoCheckList.RemoveRange(0, todoCheckList.Count);
                }

                if (txtStart.Text != "" && txtEnd.Text != "")
                {
                    step_0_GetDeliveryList();
                }
                else
                {
                    step_0_GetDeliveryList_Hour();
                }
                step_1_CopyPhoto();
                step_2_UpdateListRecord();
                step_3_UpdateDb();
            }
            catch (Exception ex)
            {
                txtBox.AppendText("Exception - " + ex.Message + "  \r\n ");
                txtBox.AppendText("Exception - " + ex.StackTrace + "  \r\n ");
                txtBox.AppendText("Exception - " + ex.Source + "  \r\n ");
            }


            txtBox.AppendText("準備結束 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");

            SaveLog();
            SaveNotify();
            Application.DoEvents();
            Application.Exit();
        }

        private void Main_Total(bool isDoMoveAndDelete)
        {
            DateTime checkStart = TimeStart.AddDays(-245);
            DateTime checkEnd = TimeStart.AddDays(-1);

            if ( txtStart.Text != "")
            {
                int a = int.Parse(txtStart.Text.Substring(0,4));
                int b = int.Parse(txtStart.Text.Substring(4, 2));
                int c = int.Parse(txtStart.Text.Substring(6, 2));
                checkStart = new DateTime(a, b, c);
            }
            if (txtEnd.Text != "")
            {
                int a = int.Parse(txtEnd.Text.Substring(0, 4));
                int b = int.Parse(txtEnd.Text.Substring(4, 2)); 
                int c = int.Parse(txtEnd.Text.Substring(6, 2));
                checkEnd = new DateTime(a, b, c);
            }

            int Gap = 1;

            txtBox.AppendText("準備開始 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");
            txtBox.AppendText("範圍 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkEnd.ToString("yyyy-MM-dd") +  "  \r\n \r\n ");
            label2.Text = "範圍 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkEnd.ToString("yyyy-MM-dd") + " ";
            Application.DoEvents();

            while (checkStart <= checkEnd)
            {
                try
                {
                    StartYYYYMMDD = checkStart.ToString("yyyyMMdd");
                    EndYYYYMMDD_1 = checkStart.AddDays(Gap).ToString("yyyyMMdd");
                    EndYYYYMMDD_2 = checkStart.AddDays(245).ToString("yyyyMMdd");

                    txtBox.AppendText("處理日期 - " + checkStart.ToString("yyyy-MM-dd") + " ~ " + checkStart.AddDays(Gap).ToString("yyyy-MM-dd") + "  \r\n ");
                    txtBox.AppendText("現在時刻 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
                    Application.DoEvents();

                    if (todoCheckList.Count > 0)
                    {
                        todoCheckList.RemoveRange(0, todoCheckList.Count);
                    }
                    step_0_GetDeliveryList();
                    step_1_CopyPhoto();
                    step_2_UpdateListRecord();
                    step_3_UpdateDb();
                    //step_4_DeleteExistImage();
                    try
                    {
                        if (isDoMoveAndDelete == true)
                        {
                            step_4_DeleteExistImage();
                        }
                    }
                    catch(Exception ex2)
                    {
                        txtBox.AppendText("Exception2 - " + ex2.Message + "  \r\n ");
                        txtBox.AppendText("Exception2 - " + ex2.StackTrace + "  \r\n ");
                        txtBox.AppendText("Exception2 - " + ex2.Source + "  \r\n ");
                    }
                }
                catch(Exception ex)
                {
                    txtBox.AppendText("Exception - " + ex.Message + "  \r\n ");
                    txtBox.AppendText("Exception - " + ex.StackTrace + "  \r\n ");
                    txtBox.AppendText("Exception - " + ex.Source + "  \r\n ");
                }

                checkStart = checkStart.AddDays(Gap+1);
            }

            txtBox.AppendText("圖檔移動總數 - " + iMoveCount.ToString() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
            Application.DoEvents();

            txtBox.AppendText("準備結束 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n");




            SaveLog();
            SaveNotify();
            Application.DoEvents();
            Application.Exit();
        }

        private void step_0_GetDeliveryList_Hour()
        {
            txtBox.AppendText("準備新增資料 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
            DeliveryPhoto.InsertDeliveryNew();
            txtBox.AppendText("新增資料完成 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
            Application.DoEvents();

            todoCheckList = DeliveryPhoto.GetDeliveryRecord_Hour();
            txtBox.AppendText("查詢總筆數 - " + todoCheckList.Count.ToString() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
            Application.DoEvents();
        }

        private void step_0_GetDeliveryList()
        {
            txtBox.AppendText("準備新增資料 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
            DeliveryPhoto.InsertDeliveryNew();
            txtBox.AppendText("新增資料完成 - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
            Application.DoEvents();
            todoCheckList = DeliveryPhoto.GetDeliveryRecord(StartYYYYMMDD, EndYYYYMMDD_1, EndYYYYMMDD_2);
            txtBox.AppendText("查詢總筆數 - " + todoCheckList.Count.ToString() + "筆 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
            Application.DoEvents();
        }

        private void step_1_CopyPhoto()
        {
            for (int i = 0; i < todoCheckList.Count; i++)
            {
                if ( i % 1000 == 50)
                {
                    txtBox.AppendText("檢查圖片 第 " + i.ToString() + " 筆  " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
                    Application.DoEvents();
                }
                //已經刪單的跳過
                if (todoCheckList[i].is_delete == "1")
                {
                    continue;
                }

                if (todoCheckList[i].latest_scan_item == "3" 
                        || todoCheckList[i].latest_scan_item == "4"
                        || todoCheckList[i].sign_form_image != "")
                {
                    if ((
                            todoCheckList[i].is_photo_digi_1_ok != true
                            && todoCheckList[i].is_photo_digi_2_ok != true
                            && todoCheckList[i].is_photo_paper_ok != true
                            )
                            || todoCheckList[i].photo_digi_1 != todoCheckList[i].sign_form_image
                            || todoCheckList[i].photo_digi_2 != todoCheckList[i].sign_field_image
                    )
                    {
                        /////////////////////////////////////////////////////////////////
                        if (
                            (
                            todoCheckList[i].is_photo_digi_1_ok != true
                            && todoCheckList[i].sign_form_image != null
                            )
                            ||
                            (
                            todoCheckList[i].is_photo_digi_1_ok == true
                            && todoCheckList[i].sign_form_image != null
                            && todoCheckList[i].photo_digi_1 != todoCheckList[i].sign_form_image
                            )
                        )
                        {
                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE.WEB_SERVICE_TO_45VM.ToString(),
                                                    todoCheckList[i].yyyymmdd,
                                                    todoCheckList[i].sign_form_image );

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._45VM_No_Partition_TO_45VM.ToString(),
                                                   todoCheckList[i].yyyymmdd,
                                                   todoCheckList[i].sign_form_image);

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._35VM_TO_45VM.ToString(),
                                                    todoCheckList[i].yyyymmdd,
                                                     todoCheckList[i].sign_form_image);
                        }
                        /////////////////////////////////////////////////////////////////
                        if (
                            (todoCheckList[i].is_photo_digi_2_ok != true 
                                && todoCheckList[i].sign_field_image != null
                            )
                            ||
                            (
                            todoCheckList[i].is_photo_digi_2_ok == true
                            && todoCheckList[i].sign_field_image != null
                            && todoCheckList[i].photo_digi_2 != todoCheckList[i].sign_field_image
                            )
                        )
                        {
                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE.WEB_SERVICE_TO_45VM.ToString(),
                                                todoCheckList[i].yyyymmdd,
                                                todoCheckList[i].sign_field_image);

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._45VM_No_Partition_TO_45VM.ToString(),
                                              todoCheckList[i].yyyymmdd,
                                              todoCheckList[i].sign_field_image);

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._35VM_TO_45VM.ToString(),
                                                todoCheckList[i].yyyymmdd,
                                                todoCheckList[i].sign_field_image);
                        }
                        ///////////////////////////////////////////////////////////////////
                        if (todoCheckList[i].is_photo_paper_ok != true)
                        {
                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE.WEB_SERVICE_TO_45VM.ToString(),
                                                    todoCheckList[i].yyyymmdd,
                                                    todoCheckList[i].check_number + ".jpg");

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._45VM_No_Partition_TO_45VM.ToString(),
                                                   todoCheckList[i].yyyymmdd,
                                                   todoCheckList[i].check_number + ".jpg");

                            step_1_CopyPhoto_sub(ENUM_COPY_PHOTO_TYPE._35VM_TO_45VM.ToString(),
                                                    todoCheckList[i].yyyymmdd,
                                                    todoCheckList[i].check_number + ".jpg");
                        }


                    }
                }
            }
        }

        private void step_1_CopyPhoto_sub(string copyType, string yyyymmdd, string fileName)
        {

            List<string> photoSourcePath = new List<string>();

            List<string> photoHomePath = new List<string>();
            photoHomePath.Add(@"\\172.30.1.45\SignPaperPhoto\Photo\");

            //.45VM的新倉庫有圖片
            //就直接返回跳下一步
            for (int i = 0; i < photoHomePath.Count; i++)
            {
                if (System.IO.File.Exists(photoHomePath[i] + yyyymmdd + @"\" + fileName))
                {
                    return;
                }
            }

            if (copyType == ENUM_COPY_PHOTO_TYPE.WEB_SERVICE_TO_45VM.ToString())
            {
                photoSourcePath.Add(@"\\172.30.1.33\d$\work\webservice\PHOTO\");

                // 2022.09.16
                //歷史已經搬過就不用了
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20210301_20211130\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20211201_20211215\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20211216_20211231\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220101_20220110\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220111_20220120\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220121_20220131\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220201_20220210\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220211_20220220\");
                //photoSourcePath.Add(@"\\172.30.1.33\f$\_勿刪_WebService照片寄放區\Work\WebService\PHOTO_20220221_20220228\");
            }
            else if (copyType == ENUM_COPY_PHOTO_TYPE._45VM_No_Partition_TO_45VM.ToString())
            {
                photoSourcePath.Add(@"\\172.30.1.45\SignPaperPhoto\_No_Partition_Photo\");
            }
            else if (copyType == ENUM_COPY_PHOTO_TYPE._35VM_TO_45VM.ToString())
            {
                photoSourcePath.Add(@"\\172.30.1.35\S3data\SignPaperPhoto\photo\");
            }



            string subPath = "";
            for (int i = 0; i < photoSourcePath.Count; i++)
            {

                if (copyType == ENUM_COPY_PHOTO_TYPE.WEB_SERVICE_TO_45VM.ToString()
                        || copyType == ENUM_COPY_PHOTO_TYPE._45VM_No_Partition_TO_45VM.ToString())
                {
                    subPath = "";
                }
                else if (copyType == ENUM_COPY_PHOTO_TYPE._35VM_TO_45VM.ToString())
                {
                    if (fileName.LastIndexOf(".") > 0)
                    {
                        subPath = buildSubPath(fileName.Substring(0, fileName.LastIndexOf(".")), "");
                    }
                    else
                    {
                        subPath = buildSubPath(fileName, "");
                    }
                }
                if (System.IO.File.Exists(photoSourcePath[i] + subPath + fileName))
                {
                    for (int j = 0; j < photoHomePath.Count; j++)
                    {
                        if (System.IO.Directory.Exists(photoHomePath[j] + yyyymmdd) == false)
                        {
                            System.IO.Directory.CreateDirectory(photoHomePath[j] + yyyymmdd);
                        }

                        System.IO.File.Copy(photoSourcePath[i] + subPath + fileName, 
                                                photoHomePath[j] + yyyymmdd + @"\" + fileName,true);
                    }

                    break;
                }
            }
        }

        private string buildSubPath(string sourceName, string resultName)
        {
            //, string extensionName
            if (sourceName.Length < 5)
            {
                return resultName;
            }
            resultName = resultName + sourceName.Substring(0, 5) + @"\";
            sourceName = sourceName.Substring(5);

            return buildSubPath(sourceName, resultName);
        }

        private void step_2_UpdateListRecord()
        {
            List<string> photoHomePath = new List<string>();
            photoHomePath.Add(@"\\172.30.1.45\SignPaperPhoto\Photo\");

            for (int i = 0; i < todoCheckList.Count; i++)
            {
                if (i % 3000 == 50)
                {
                    txtBox.AppendText("準備更新資料 第 " + i.ToString() + " 筆  " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
                    Application.DoEvents();
                }

                //主表有刪除紀錄(is_delete) 但簽單表還沒有刪除紀錄(is_delete2) => 更新刪除紀錄+現在時間
                //已經被刪除的 不需要管簽單有沒有
                if (todoCheckList[i].is_delete == "1" 
                    && todoCheckList[i].is_delete2 != "1")
                {
                    todoCheckList[i].is_delete = "1";
                    todoCheckList[i].udate = DateTime.Now;
                    continue;
                }

                //可能也許疑似有  已經被銷單的 又再度被恢復
                if (todoCheckList[i].is_delete == "0"
                        && todoCheckList[i].is_delete2 != "0")
                {
                    todoCheckList[i].is_delete = "0";
                    todoCheckList[i].udate = DateTime.Now;
                }

                //圖片檢核邏輯
                //
                //如果是APP的簽單
                //應該在 sign_form_image / sign_field_image 有資料
                //
                //如果是掃描簽單
                //只能直接硬上貨號查查看
                //
                for ( int j = 0; j < photoHomePath.Count; j++)
                {
                    if (
                        (todoCheckList[i].is_photo_paper_ok != true || todoCheckList[i].is_photo_paper_ok == null )
                        &&(System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].check_number + ".jpg")
                        && todoCheckList[i].check_number.Length > 5 )
                        )
                    {
                        todoCheckList[i].is_photo_paper_ok = true;
                        todoCheckList[i].photo_paper = todoCheckList[i].check_number + ".jpg";
                        todoCheckList[i].udate = DateTime.Now;
                    }

                    if (todoCheckList[i].is_photo_digi_1_ok == true 
                        && todoCheckList[i].photo_digi_1 != todoCheckList[i].sign_form_image
                        && System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].sign_form_image)
                        && todoCheckList[i].sign_form_image.Length > 5
                        )
                    {
                        todoCheckList[i].is_photo_digi_1_ok = true;
                        todoCheckList[i].photo_digi_1 = todoCheckList[i].sign_form_image;
                        todoCheckList[i].udate = DateTime.Now;
                    }

                    if (
                        (todoCheckList[i].is_photo_digi_1_ok != true || todoCheckList[i].is_photo_digi_1_ok == null )
                        && todoCheckList[i].sign_form_image != null
                        && System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].sign_form_image )
                        && todoCheckList[i].sign_form_image.Length > 5 )
                    {
                        todoCheckList[i].is_photo_digi_1_ok = true;
                        todoCheckList[i].photo_digi_1 = todoCheckList[i].sign_form_image;
                        todoCheckList[i].udate = DateTime.Now;
                    }

                    if (todoCheckList[i].is_photo_digi_2_ok == true
                        && todoCheckList[i].photo_digi_2 != todoCheckList[i].sign_field_image
                        && System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].sign_field_image)
                        && todoCheckList[i].sign_field_image.Length > 5
                    )
                    {
                        todoCheckList[i].is_photo_digi_2_ok = true;
                        todoCheckList[i].photo_digi_2 = todoCheckList[i].sign_field_image;
                        todoCheckList[i].udate = DateTime.Now;
                    }

                    if (
                        (todoCheckList[i].is_photo_digi_2_ok != true || todoCheckList[i].is_photo_digi_2_ok == null )
                        && todoCheckList[i].sign_field_image != null
                        && System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].sign_field_image)
                        && todoCheckList[i].sign_field_image.Length > 5 )
                    {
                        todoCheckList[i].is_photo_digi_2_ok = true;
                        todoCheckList[i].photo_digi_2 = todoCheckList[i].sign_field_image;
                        todoCheckList[i].udate = DateTime.Now;
                    }
                }


            }
        }

        private void step_3_UpdateDb()
        {
            txtBox.AppendText("開始更新資料庫 - "  + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n ");
            Application.DoEvents();
            int tCount = DeliveryPhoto.UpdateRecord(todoCheckList, TimeStart);
            txtBox.AppendText("更新筆數 - " + tCount.ToString() + "  \r\n \r\n ");
            iTotalCount += todoCheckList.Count();
            iUpdateCount += tCount;
        }

        private void step_4_DeleteExistImage()
        {
            txtBox.AppendText("準備刪除圖片(超過35天搬移到暫存區) - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
            Application.DoEvents();

            List<string> photoSourcePath = new List<string>();
            photoSourcePath.Add(@"\\172.30.1.33\d$\work\webservice\PHOTO\");
            photoSourcePath.Add(@"\\172.30.1.45\SignPaperPhoto\_No_Partition_Photo\");

            List<string> photoHomePath = new List<string>();
            photoHomePath.Add(@"\\172.30.1.45\SignPaperPhoto\Photo\");

            bool isMove = false; 

            for (int i = 0; i < todoCheckList.Count; i++)
            {
                isMove = false;
                if ( int.Parse(todoCheckList[i].yyyymmdd) > int.Parse(DateTime.Now.AddDays(-35).ToString("yyyyMMdd")) )
                {
                    //最近35天的暫時不搬家
                    continue;
                }

                if (i % 3000 == 50)
                {
                    txtBox.AppendText("檢查圖片是否過期 第 " + i.ToString() + " 筆  " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  \r\n \r\n ");
                    Application.DoEvents();
                }

                for (int j = 0; j < photoHomePath.Count; j++)
                {
                    if (todoCheckList[i].is_photo_paper_ok == true )
                    {
                        if (System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].photo_paper) == true)
                        {
                            for (int k = 0; k < photoSourcePath.Count; k++)
                            {
                                if (System.IO.File.Exists(photoSourcePath[k] + todoCheckList[i].photo_paper) == true)
                                {
                                    if (System.IO.File.GetCreationTime(photoSourcePath[k] + todoCheckList[i].photo_paper) 
                                                <= TimeStart.AddDays(-35) )
                                    {
                                        isMove = true;
                                        step4_MoveToTemp_sub(photoSourcePath[k] , todoCheckList[i].photo_paper);
                                    }

                                }
                            }
                        }
                    }
                    if (todoCheckList[i].is_photo_digi_1_ok == true)
                    {
                        if (System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].photo_digi_1) == true)
                        {
                            for (int k = 0; k < photoSourcePath.Count; k++)
                            {
                                if (System.IO.File.Exists(photoSourcePath[k] + todoCheckList[i].photo_digi_1) == true)
                                {
                                    if (System.IO.File.GetCreationTime(photoSourcePath[k] + todoCheckList[i].photo_digi_1)
                                                <= TimeStart.AddDays(-35))
                                    {
                                        isMove = true;
                                        step4_MoveToTemp_sub(photoSourcePath[k], todoCheckList[i].photo_digi_1);
                                    }

                                }
                            }
                        }
                    }
                    if (todoCheckList[i].is_photo_digi_2_ok == true)
                    {
                        if (System.IO.File.Exists(photoHomePath[j] + todoCheckList[i].yyyymmdd + @"\" + todoCheckList[i].photo_digi_2) == true)
                        {
                            for (int k = 0; k < photoSourcePath.Count; k++)
                            {
                                if (System.IO.File.Exists(photoSourcePath[k] + todoCheckList[i].photo_digi_2) == true)
                                {
                                    if (System.IO.File.GetCreationTime(photoSourcePath[k] + todoCheckList[i].photo_digi_2)
                                                <= TimeStart.AddDays(-35))
                                    {
                                        isMove = true;
                                        step4_MoveToTemp_sub(photoSourcePath[k], todoCheckList[i].photo_digi_2);
                                    }

                                }
                            }
                        }
                    }
                }

                if (isMove == true)
                {
                    iMoveCount += 1;
                }

            }

        }

        private void step4_MoveToTemp_sub(string folderName , string fileName)
        {
            List<string> photoDeletePath = new List<string>();
            photoDeletePath.Add(@"\\172.30.1.45\SignPaperPhoto\zzDelete\");

            try
            {
                System.IO.File.Move(folderName+ fileName, photoDeletePath[0] + fileName);
            }
            catch(Exception ex)
            {

            }

        }

        private void SaveLog()
        {
            try
            {
                if (!System.IO.Directory.Exists("Log"))
                {
                    System.IO.Directory.CreateDirectory("Log");
                }
                System.IO.File.WriteAllText("Log/" + DateTime.Now.ToString("yyyyMMddHHmm") + "__log.txt", txtBox.Text.ToString());
            }
            catch (Exception ex)
            {

            }

        }

        private void SaveNotify()
        {
            //改成DB端固定發紀錄
            return; 

            if (isHour == false)
            {
                //白天時段再發送LOG
                if (DateTime.Now.Hour >= 7 && DateTime.Now.Hour <= 19)
                {

                    DeliveryPhoto.InsertRecord(
                                 new JobRecord("10000", "005", "OK", "簽單搬家工程(近月批次)", "\r\n總筆數" + iTotalCount.ToString() + "筆 ; \r\n更新筆數" + iUpdateCount.ToString() + "筆 ; \r\n圖檔刪除筆數" + iMoveCount.ToString() + "筆 ", "", 1, "")
                             );
                }
                else
                {
                    DeliveryPhoto.InsertRecord(
                                 new JobRecord("10000", "005", "OK", "簽單搬家工程(近月批次)", "\r\n總筆數" + iTotalCount.ToString() + "筆 ; \r\n更新筆數" + iUpdateCount.ToString() + "筆 ; \r\n圖檔刪除筆數" + iMoveCount.ToString() + "筆 ", "", 0, "")
                             );
                }
            }
            else
            {
                DeliveryPhoto.InsertRecord(
                                 new JobRecord("10000", "005", "OK", "簽單搬家工程", " \r\n總筆數" + iTotalCount.ToString() + "筆 ; \r\n更新筆數" + iUpdateCount.ToString() + "筆 ;", "", 0, "")
                             );
            }
        }
    }
}
