﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckImageAP.DB;
namespace CheckImageAP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<CheckNumberObj> listCheckNumber;

        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://s3.console.aws.amazon.com/s3/buckets/storage-for-station?region=us-east-2&tab=objects");
        }

        bool isContinue = true;
        int iCount = 0;

        string strDirectoryPath = "";
        string strFilePath = "";
        string strFilePath_S3 = "";
        string strSaveName = "";

        bool isS3ImageOK = false;

        private void checkExists(int iIndex)
        {
            string strCheckNumber = listCheckNumber[iIndex].check_number;
            string strSignImage = listCheckNumber[iIndex].sign_field_image;
            string strFullImage = listCheckNumber[iIndex].sign_form_image;

            strDirectoryPath = "";
            strFilePath = "";
            strFilePath_S3 = "";
            strSaveName = "";


            //2022/01/17
            // 暫時區段
            // 紀錄 2021/12 有電子簽收的簽單
            // 是否有實體簽單路徑

            #region calc_second
            /*
            //APP
            strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "");
            strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
            strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
            strSaveName = strFullImage;

            if (System.IO.File.Exists(strFilePath))
            {
                listCheckNumber[iIndex]._35Exists = 1;
                listCheckNumber[iIndex].filePath = strFilePath;
            }


            //實體掃描 
            strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "");
            strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "") + strCheckNumber + ".jpg";
            strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "", strCheckNumber, "") + strCheckNumber + @".jpg";
            strSaveName = strCheckNumber + ".jpg";

            if (System.IO.File.Exists(strFilePath))
            {
                listCheckNumber[iIndex]._35Exist_seconds = 1;
                listCheckNumber[iIndex].filePath_second = strFilePath;
            }

            return;
            */
            #endregion



            if (strSignImage == "" || strFullImage == "" )
            {
                //實體掃描 
                strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath( @"\" , "" ,  strCheckNumber, "");
                strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath( @"\",  "",strCheckNumber,"") + strCheckNumber + ".jpg";
                strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "", strCheckNumber, "") + strCheckNumber + @".jpg";
                strSaveName = strCheckNumber + ".jpg";
            }
            else
            {
                //APP
                strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".") ), "");
                strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".") ),"") + strFullImage;
                strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".") ), "") + strFullImage;
                strSaveName = strFullImage;
            }

            //  \\52.15.86.18\storage-for-station\SignPaperPhoto
            //  https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/10305/00844/103050084406.jpg




            if ( System.IO.File.Exists(strFilePath) )
            {
                listCheckNumber[iIndex]._35Exists = 1;
                listCheckNumber[iIndex].filePath = strFilePath;
            }
            else
            {
                return;
                //if (strFullImage != "")
                //{
                //    if (strDirectoryPath.LastIndexOf(@"\") == strDirectoryPath.Length -1)
                //    {
                //        strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "");
                //        strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
                //        strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
                //        strSaveName = strFullImage;


                //        string strDirTemp = getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "");
                //        strDirTemp = strDirTemp.Substring(0, strDirTemp.Length - 1);

                //        if ((strDirTemp.LastIndexOf(@"\") + 1 + 5).Equals(strDirTemp.Length))
                //        {
                //            strDirTemp = strDirTemp.Substring(0, strDirTemp.LastIndexOf(@"\") + 1);

                //            strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + strDirTemp;
                //            strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + strDirTemp + strFullImage;

                //            strSaveName = strFullImage;

                //            if (System.IO.File.Exists(strFilePath))
                //            {
                //                listCheckNumber[iIndex]._35Exists = 1;
                //                listCheckNumber[iIndex].filePath = strFilePath;
                //            }

                //        }
                //    }
                //}
                //return;

                //strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "");
                //strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
                //strFilePath_S3 = @"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + getFilePath(@"/", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
                //strSaveName = strFullImage;

                txtBox.AppendText("改去S3查查看的 CheckNumber " + listCheckNumber[iIndex].check_number +  " \r\n ");
                Application.DoEvents();

                //<img style="-webkit-user-select: none;margin: auto;cursor: zoom-in;background-color: hsl(0, 0%, 90%);transition: background-color 300ms;" src=
                ////"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/10305/00822/103050082203.jpg" width="999" height="481">

                try
                {

                    //var thread = new Thread(() =>
                    //{
                    //    webBrowser1.Navigate(@"https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/10305/00822/103050082203.jpg");
                    //});

                    //thread.SetApartmentState(ApartmentState.STA);
                    //thread.Start();

                    //webBrowser1.Navigate("https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/10305/00822/103050082203.jpg");// (strFilePath_S3);
                    //webBrowser1.Navigate(strFilePath_S3);

                    iCount = 0;
                    isContinue = false;
                    isS3ImageOK = false;
                    
                    webBrowser1.Navigate(strFilePath_S3);

                    while ( isContinue == false && iCount < 10000)
                    {
                        System.Threading.Thread.Sleep(100);
                        Application.DoEvents();
                        iCount += 1;
                    }

                    //if (thread.IsAlive)
                    //{
                    //    thread.Abort();
                    //}
                }
                catch(Exception ex)
                {

                }


                if (isS3ImageOK == false)
                {
                    listCheckNumber[iIndex]._35Exists = 0;
                    listCheckNumber[iIndex].filePath = "";

                    listCheckNumber[iIndex]._S3Exists = -1;
                    listCheckNumber[iIndex]._S3filePath = "";
                    return;
                }

                try
                {
                    // S3 有檔案 所以先建立 .35 的目錄
                    System.IO.Directory.CreateDirectory(strDirectoryPath);
                    // 從 S3 複製檔案回本地端
                    System.IO.File.Copy(strSaveName, strFilePath, true);

                    //回到本來的判斷
                    //再加上 S3的註記
                    if (System.IO.File.Exists(strFilePath))
                    {
                        listCheckNumber[iIndex]._35Exists = 1;
                        listCheckNumber[iIndex].filePath = strFilePath;

                        listCheckNumber[iIndex]._S3Exists = 1;
                        listCheckNumber[iIndex]._S3filePath = strFilePath_S3;
                    }
                }
                catch(Exception ex)
                {

                }

            }
        }

        public string getFilePath( string strGap , string strType , string strPreName , string strLastName)
        {
            if (strGap != @"\" && strGap != @"/")
            {
                strGap = @"\";
            }
            //  strGap = "/" or "\"
            if (strPreName.Length < 5)
            {
                return strLastName;
            }
            else
            {
                //strLastName = strLastName + strPreName.Substring(0, 5) + strGap; // + @"\";
                strLastName = strLastName + strPreName.Substring(0, 5) + strGap; // + @"\";
                strPreName = strPreName.Substring(5);
            }

            return getFilePath(strGap , strType, strPreName, strLastName); 

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string strStart = "";
            string strEnd = "";

            strStart = int.Parse(textBox1.Text).ToString().Trim();
            strEnd = int.Parse(textBox2.Text).ToString().Trim();

            if ( strStart == "" || strEnd == "")
            {
                strStart = "20210101";
                strEnd = "20220101";
            }

            label1.Text = strStart + " ~" + strEnd; 

            txtBox.AppendText("START " + label1.Text + " \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            Application.DoEvents();
            listCheckNumber = new List<CheckNumberObj>();
            listCheckNumber = DBClass.getCheckNumberList(strStart, strEnd);

            txtBox.AppendText(listCheckNumber.Count.ToString() + "筆 \r\n ");
            for (int i = 0; i < listCheckNumber.Count; i++)
            {
                if (i % 200 == 0)
                {
                    txtBox.AppendText("正在執行檢查 第" + i.ToString() + "筆 \r\n ");
                    System.Threading.Thread.Sleep(1);
                    Application.DoEvents();
                }

                checkExists(i);

                if ( i > 0 && i % 10000 == 0 )
                {
                    System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmm") + "__log.txt", txtBox.Text.ToString());
                    txtBox.Text = "";
                    txtBox.AppendText("START \r\n ");
                    txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
                    Application.DoEvents();
                }
            }
            txtBox.AppendText("======= \r\n ");
            for (int i = 0; i < listCheckNumber.Count; i++)
            {
                if (i % 200 == 0)
                {
                    txtBox.AppendText("正在執行更新 第" + i.ToString() + "筆 \r\n ");
                    System.Threading.Thread.Sleep(1);
                    Application.DoEvents();
                }
                DBClass.updateRecord(listCheckNumber[i]);

                if (i > 0 && i % 50000 == 0)
                {
                    System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmmss") + "__log.txt", txtBox.Text.ToString());
                    txtBox.Text = "";
                    txtBox.AppendText("START \r\n ");
                    txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
                    Application.DoEvents();
                }
            }

            System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmmss") + "__log.txt", txtBox.Text.ToString());
            Application.Exit();

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (webBrowser1.ReadyState == WebBrowserReadyState.Complete)
            {
                if ( 1==1)
                {
                    try
                    {
                        string strImageScr = webBrowser1.Document.GetElementsByTagName("image")[0].GetAttribute("src");

                        WebClient wc = new WebClient();
                        byte[] bytes = wc.DownloadData(strImageScr);
                        Bitmap b = new Bitmap(new MemoryStream(bytes));

                        b.Save(strSaveName);

                        isS3ImageOK = true;
                    }
                    catch(Exception ex)
                    {
                        isS3ImageOK = false;
                    }

                    isContinue = true;
                }
            }
        }

    }
}
