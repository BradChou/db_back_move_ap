﻿using CheckImageAP.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckImageAP
{
    public partial class Form2 : Form
    {

        /*
查詢SQL語法

SELECT [cdate],
  isnull([DeliveryOKTotal],'') as '已配達數',
  isnull([Delivery_Digi],'') as '電子簽收',
  isnull([Delivery_Photo],'') as '拍照簽收',
  isnull([Delivery_Paper],'') as '紙本簽收',
  isnull([Delivery_Paper_99],'') as '轉聯運簽收',
  isnull([DeliveryButNoPaper_Paper],'') as '紙本簽收 但是沒有紙本',
  isnull([DeliveryButNoPaper_Paper_99],'') as '轉聯運簽收 但沒有紙本',
  isnull([Delivery_Mix],'') as '重複 (數位簽收同時存在紙本)'
FROM [dbo].[zzzzCheckCalcResult]
Where cdate >= '20220401'
Order by cdate

        每天跑一次排程
        計算最近4x天的託運單
        有沒有收到簽單
        */
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            txtBox.AppendText("等候開始  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            System.Threading.Thread.Sleep(1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false; 
            if (button1.Enabled == true)
            {
                try
                {
                    button1.Enabled = false;
                    MainProj();
                    System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmm") + "__log.txt", txtBox.Text.ToString());
                    //txtBox.Text = "";
                }
                catch(Exception ex)
                {
                    string strError = "";
                    strError += ex.Message;
                    strError += ex.Source ;
                    strError += ex.StackTrace;
                    System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmm") + "__log.txt", strError.ToString());

                    //txtBox.Text = "";
                }
                button1.Enabled = true;
            }
                
            Application.DoEvents();
            Application.Exit();
        }


        public List<CheckObj> listCheckNumber;

        bool isContinue = true;
        int iCount = 0;

        string strDirectoryPath = "";
        string strFilePath = "";
        string strSaveName = "";

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            if (button1.Enabled == true)
            {
                try
                {
                    button1.Enabled = false;
                    MainProj();

                    txtBox.AppendText("全部完成  \r\n ");
                    txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
                    System.Threading.Thread.Sleep(1);

                    System.IO.File.WriteAllText(DateTime.Now.ToString("yyyyMMddHHmm") + "__log.txt", txtBox.Text.ToString());
                    //txtBox.Text = "";
                }
                catch (Exception ex)
                {

                }
                button1.Enabled = true;
            }

            Application.Exit();
        }

        private void MainProj()
        {
            txtBox.AppendText("START  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");

            txtBox.AppendText("準備重拉資料  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            System.Threading.Thread.Sleep(1);
            Application.DoEvents();
            //重拉資料
            DBClass.ReBuildDbImageData("STEP_1");

            txtBox.AppendText("準備重建索引  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            System.Threading.Thread.Sleep(1);
            Application.DoEvents();
            //重建索引
            DBClass.ReBuildDbImageData("STEP_2");


            txtBox.AppendText("準備重新找簽單圖片  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            System.Threading.Thread.Sleep(1);
            Application.DoEvents();
            listCheckNumber = new List<CheckObj>();
            listCheckNumber = DBClass.getCheckList_STEP3();


            txtBox.AppendText(listCheckNumber.Count.ToString() + "筆 \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            Application.DoEvents();

            for (int i = 0; i < listCheckNumber.Count; i++)
            {
                if (i % 200 == 0)
                {
                    txtBox.AppendText("正在執行檢查 第" + i.ToString() + "筆 \r\n ");
                    System.Threading.Thread.Sleep(1);
                    Application.DoEvents();
                }

                checkExists(i);

            }

            txtBox.AppendText("======= \r\n ");

            for (int i = 0; i < listCheckNumber.Count; i++)
            {
                if (i % 200 == 0)
                {
                    txtBox.AppendText("正在執行更新 第" + i.ToString() + "筆 \r\n ");
                    System.Threading.Thread.Sleep(1);
                    Application.DoEvents();
                }
                DBClass.updateObj(listCheckNumber[i]);
            }

            txtBox.AppendText("準備重算數據  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            System.Threading.Thread.Sleep(1);
            //重算數據
            DBClass.ReBuildDbImageData("STEP_4");

        }

        private void checkExists(int iIndex)
        {
            string strCheckNumber = listCheckNumber[iIndex].check_number;
            string strSignImage = listCheckNumber[iIndex].sign_field_image;
            string strFullImage = listCheckNumber[iIndex].sign_form_image;

            strDirectoryPath = "";
            strFilePath = "";
            strSaveName = "";


            //2022/01/17
            // 暫時區段
            // 紀錄 2021/12 有電子簽收的簽單
            // 是否有實體簽單路徑


            if (strSignImage == "" || strFullImage == "")
            {
                //實體掃描 
                strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "");
                strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "") + strCheckNumber + ".jpg";
                strSaveName = strCheckNumber + ".jpg";
            }
            else
            {
                //APP
                strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "");
                strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "APP", strFullImage.Substring(0, strFullImage.LastIndexOf(".")), "") + strFullImage;
                strSaveName = strFullImage;
            }

            if (System.IO.File.Exists(strFilePath))
            {
                listCheckNumber[iIndex].ImageType = "OK";
            }


            if (listCheckNumber[iIndex].SignType != "紙本")
            {
                //電子簽名也要看看紙本實體掃描 
                strDirectoryPath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "");
                strFilePath = @"\\172.30.1.35\S3data\SignPaperPhoto\photo\" + getFilePath(@"\", "", strCheckNumber, "") + strCheckNumber + ".jpg";
                strSaveName = strCheckNumber + ".jpg";

                if (System.IO.File.Exists(strFilePath))
                {
                    listCheckNumber[iIndex].ImageType = "OK";
                    listCheckNumber[iIndex].ImageType_Second = "OK";
                }

            }

            return;
        }


        public string getFilePath(string strGap, string strType, string strPreName, string strLastName)
        {
            if (strGap != @"\" && strGap != @"/")
            {
                strGap = @"\";
            }
            //  strGap = "/" or "\"
            if (strPreName.Length < 5)
            {
                return strLastName;
            }
            else
            {
                //strLastName = strLastName + strPreName.Substring(0, 5) + strGap; // + @"\";
                strLastName = strLastName + strPreName.Substring(0, 5) + strGap; // + @"\";
                strPreName = strPreName.Substring(5);
            }

            return getFilePath(strGap, strType, strPreName, strLastName);

        }


    }
}
