﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;

namespace CheckImageAP.DB
{
    public class DBClass
    {
        public const string strConn = "Data Source=172.30.1.34;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=lyAdmin;Password=P@ssw0rd!!!";
		public const string strConn_Address = "Data Source=172.30.1.34;Initial Catalog=JunFuAddress;Persist Security Info=True;User ID=lyAdmin;Password=P@ssw0rd!!!";

		public static List<CheckNumberObj> getCheckNumberList(string strStart, string strEnd)
        {
            List<CheckNumberObj> returnData = new List<CheckNumberObj>();

            try
            {
                using (var cn = new SqlConnection(strConn))
                {
					/*

Insert Into zzCheckImageRecord
Select * 
From 
(
	Select A.request_id , A.check_number , isnull(B.sign_field_image,'') as sign_field_image , isnull(B.sign_form_image,'') as sign_form_image , 
					0 as _35Exists , '' as filePath , 0 as tt , '' as yy, 0 as tt2 , '' as yy2
	From 
	(
	Select distinct request_id , check_number From tcDeliveryRequests with (nolock)
	Where cdate >= '2020-01-01'
	and cdate <= '2021-12-31'
	and latest_scan_item in ('3','4')
	and Less_than_truckload = '1'
	and cancel_date is null
	) A 
	left join 
	(
	Select  check_number , sign_field_image , sign_form_image From ttDeliveryScanLog with (nolock)
	Where cdate >= '2020-01-01'
	and cdate <= '2021-12-31'
	and scan_item in ('4')
	and log_id in (
					Select  max(log_id) as log_id From ttDeliveryScanLog with (nolock)
					Where cdate >= '2020-01-01'
					and cdate <= '2021-12-31'
					and scan_item in ('4')
					Group by check_number
				)
	) B on A.check_number = B.check_number
) T
Where T.request_id not in ( Select request_id From zzCheckImageRecord)
Order by T.request_id 


Select request_id , check_number , sign_field_image , sign_form_image , _35Exists , filePath , isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord
Where _35Exists = 0
and check_number in (
						Select check_number From tcDeliveryRequests where cdate >= '2021-07-06' and  cdate <= '2021-12-25'
                    )
Order by request_id






Select	cdate , 
		sum(status_lose) as status_lose , 
		sum(status_ok) as status_ok , 
		sum(status_ok_but_copy_s3) as status_ok_but_copy_s3 , 
		sum(status_lose) + sum(status_ok) as total_count , 
		Cast ( 100.0 * sum(status_ok) / (sum(status_lose) + sum(status_ok)) as decimal(10,2) ) '成功百分比'
From 
(
Select convert( nvarchar(8) , cdate , 112 ) as cdate , count(*) as status_lose , 0 as status_ok , 0 as status_ok_but_copy_s3  From tcDeliveryRequests with (nolock)
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 0 
					) 
group by convert( nvarchar(8) , cdate , 112 ) 
union all
Select convert( nvarchar(8) , cdate , 112 ) as cdate, 0 as status_lose , count(*) as status_ok , 0 as status_ok_but_copy_s3  From tcDeliveryRequests with (nolock)
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 1 
					) 
group by convert( nvarchar(8) , cdate , 112 ) 
union all
Select convert( nvarchar(8) , cdate , 112 ) as cdate, 0 as status_lose , 0 as status_ok , count(*) as status_ok_but_copy_s3  
From tcDeliveryRequests with (nolock)
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 1 and _S3Exists = 1
					) 
group by convert( nvarchar(8) , cdate , 112 ) 
) A
Group by cdate
Order by cdate



Select	station_scode , station_name , 
		sum(status_lose) as status_lose , 
		sum(status_ok) as status_ok , 
		sum(status_ok_but_copy_s3) as status_ok_but_copy_s3 , 
		sum(status_lose) + sum(status_ok) as total_count , 
		Cast ( 100.0 * sum(status_ok) / (sum(status_lose) + sum(status_ok)) as decimal(10,2) ) '成功百分比'
From 
(
Select station_scode , station_name , count(*) as status_lose , 0 as status_ok , 0 as status_ok_but_copy_s3  
From tcDeliveryRequests a with (nolock) 
inner join (Select station_scode , station_name From tbStation) b on a.area_arrive_code = b.station_scode
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 0 
					) 
group by station_scode , station_name
union all
Select station_scode , station_name, 0 as status_lose , count(*) as status_ok , 0 as status_ok_but_copy_s3  
From tcDeliveryRequests a with (nolock) 
inner join (Select station_scode , station_name From tbStation) b on a.area_arrive_code = b.station_scode
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 1 
					) 
group by station_scode , station_name
union all
Select station_scode , station_name, 0 as status_lose , 0 as status_ok , count(*) as status_ok_but_copy_s3  
From tcDeliveryRequests a with (nolock) 
inner join (Select station_scode , station_name From tbStation) b on a.area_arrive_code = b.station_scode
Where cdate >= '2021/01/01' -- cdate >= '2021/07/07' and cdate <= '2021/12/10'
and check_number in ( 
					Select check_number 
					From zzCheckImageRecord 
					Where _35Exists = 1 and _S3Exists = 1
					) 
group by station_scode , station_name
) A
Group by station_scode , station_name
Order by station_scode , sum(status_lose) + sum(status_ok) desc




Select request_id , check_number , sign_field_image , sign_form_image , _35Exists , filePath , isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord with (nolock)
Where sign_form_image <> '' and _35Exists = 0
and check_number in (
						Select check_number From tcDeliveryRequests with (nolock) where cdate >= '2021-11-01' and  cdate <= '2022-01-01'
                    )

Order by check_number

Select request_id , check_number , sign_field_image , sign_form_image , _35Exists , filePath , isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord with (nolock)
Where _35Exists = 0 and _S3Exists <> -1  
and check_number in (
						Select check_number From tcDeliveryRequests with (nolock) where cdate >= '2021-01-01' and  cdate <= '2022-01-01'
                    )
Order by check_number



Select	request_id , 
		check_number , 
		sign_field_image , sign_form_image , 
		_35Exists , filePath , 
		_35Exist_seconds , filePath_second ,
		isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord with (nolock)
Where _35Exists = 1
and check_number in (
						Select check_number From tcDeliveryRequests with (nolock) where cdate >= '" + strStart + @"' and  cdate <= '" + strEnd + @"'
                    )
and check_number in (
						Select CheckNumber From SignImage
						Where CheckNumber in (
											Select check_number  From tcDeliveryRequests where cdate >= '" + strStart + @"' and  cdate <= '" + strEnd + @"'
											)
						)
Order by check_number


Select request_id , 
		check_number , 
		sign_field_image , sign_form_image , 
		_35Exists , filePath , 
		isnull(_35Exist_seconds,'')  as _35Exist_seconds , isnull(filePath_second,'') as filePath_second ,
		isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord with (nolock)
Where _35Exists = 0 
and check_number in (
						Select check_number From tcDeliveryRequests with (nolock) where cdate >= '" + strStart + @"' and  cdate <= '" + strEnd + @"'
                    )
Order by check_number

                    */

					//strStart, strEnd
					//and left(check_number,5) in ( '99000', '98000' )  
					//and left(check_number,5) in ( '70006', '50006')  
					//and left(check_number,5) in ( '80100','80200','88000','88005','88006' ) 



					string strSQL = @"
Select request_id , 
		check_number , 
		sign_field_image , sign_form_image , 
		_35Exists , filePath , 
		isnull(_35Exist_seconds,'')  as _35Exist_seconds , isnull(filePath_second,'') as filePath_second ,
		isnull(_S3Exists,'') as _S3Exists , isnull(_S3filePath,'') as _S3filePath
From zzCheckImageRecord with (nolock)
Where _35Exists = 0 
and check_number in (
						Select check_number From tcDeliveryRequests with (nolock) where cdate >= '" + strStart + @"' and  cdate <= '" + strEnd + @"'
                    )
Order by check_number
";
                    returnData = (List<CheckNumberObj>)cn.Query<CheckNumberObj>(strSQL);
                }
            }
            catch (Exception e)
            {

            }

            return returnData;
        }

        public static void updateRecord(CheckNumberObj tobj)
        {
     
            try
            {
                using (var cn = new SqlConnection(strConn))
                {
                 

                    string strSQL = @"
    Update zzCheckImageRecord
    Set _35Exists = @_35Exists , 
        filePath = @filePath , 
		_35Exist_seconds = @_35Exist_seconds ,
		filePath_second = @filePath_second , 
        _S3Exists = @_S3Exists , 
        _S3filePath = @_S3filePath
    Where request_id = @request_id  ";
                    cn.ExecuteScalar(strSQL, new { request_id = tobj.request_id , 
                                                    _35Exists = tobj._35Exists , 
													filePath = tobj.filePath ,
													_35Exist_seconds = tobj._35Exist_seconds ,
													filePath_second = tobj.filePath_second,
													_S3Exists = tobj._S3Exists, 
													_S3filePath = tobj._S3filePath
                                                });
                }
            }
            catch (Exception e)
            {

            }
        }

		public static List<AddressSource> getAddressSourceList(string yyyyMMdd)
		{
			if (yyyyMMdd.Equals(""))
            {
				yyyyMMdd = DateTime.Now.AddDays(-10).ToString("yyyyMMdd");
            }
			List<AddressSource> returnData = new List<AddressSource>();

			try
			{
				using (var cn = new SqlConnection(strConn_Address))
				{
					string strSQL = @"
Select id , InitialAddress , CreateTime
From [dbo].[AddressResult]
Where CreateTime >= @CreateTime
Order by id
";
					returnData = (List<AddressSource>)cn.Query<AddressSource>(strSQL , new { CreateTime = yyyyMMdd });
				}
			}
			catch (Exception e)
			{

			}

			return returnData;
		}



		public static void ReBuildDbImageData(string SQL_TYPE)
        {
			SQL_TYPE = SQL_TYPE.ToUpper();
			try
			{
				using (var cn = new SqlConnection(strConn))
				{
					string strSQL = "";
						
					if (SQL_TYPE.Equals("STEP_1"))
                    {
						strSQL = @"
	Declare @now datetime
	Set @now = getdate() 
	Set @now = Dateadd( day , -3 , @now ) 

	Declare @start datetime 
	Set @start = Dateadd( day , -95 , @now ) 

	Declare @YYYYMMDD_START nvarchar(8) 
	Declare @YYYYMMDD_END nvarchar(8) 

	Set @YYYYMMDD_START = convert ( nvarchar(8) , @start , 112 )
	Set @YYYYMMDD_END = convert ( nvarchar(8) , @now , 112 )


	--刪除可能會更新的歷史紀錄
	Delete From zzzzCheck
	Where cdate >= @YYYYMMDD_START


	Insert into zzzzCheck (	[cdate],[check_number],[area_arrive_code],
							[DeliveryType],[SignType],[ImageType],[ImageType_Second],
							[sign_field_image],[sign_form_image])
		Select	a.cdate , 
				a.check_number , 
				a.area_arrive_code , 
				DeliveryType , 
				case 
					When a.DeliveryType = '在途' then '--'
					When b.SignType is null then '紙本'
					When b.SignType = '-1' then '不明'
					else b.SignType
				End as SignType ,
				'' as ImageType , 
				'' as [ImageType_Second] , 
				c.sign_field_image , 
				c.sign_form_image
		From 
		(
		Select	check_number ,
				convert ( nvarchar(8) , cdate , 112 )  as cdate ,
				area_arrive_code ,
				case 
					When latest_scan_item in ('3','4') then '已配達'
					else '在途'
				End as DeliveryType 
		From tcDeliveryRequests
		Where cdate >= @YYYYMMDD_START and cdate < @YYYYMMDD_END
		and ( cancel_date is null or cancel_date = '' )
		and ship_date is not null
		) a 
		left join 
		(
		Select	CheckNumber as check_number , 
				case 
				   when SignType = '1' then '電子簽名' 
				   when SignType = '2' then 'QRcode簽名'
				   when SignType = '3' then '簽單拍照'
				   else '-1' 
				end SignType  
		From SignImage
		) b on a.check_number = b.check_number
		left join 
		(
		Select  check_number , sign_field_image , sign_form_image From ttDeliveryScanLog with (nolock)
		Where cdate >= @YYYYMMDD_START
		and scan_item in ('4')
		and log_id in (
						Select  max(log_id) as log_id From ttDeliveryScanLog with (nolock)
						Where cdate >= @YYYYMMDD_START
						and scan_item in ('4')
						and sign_field_image <> '' 
						and sign_form_image <> ''
						Group by check_number
					)
		) c on A.check_number = c.check_number
		Order by a.cdate

	";
                    }
					else if (SQL_TYPE.Equals("STEP_2"))
					{
						strSQL = @"
ALTER INDEX [NonClusteredIndex-20220519-130027] ON [dbo].[zzzzCheck] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
";
                    }
					else if (SQL_TYPE.Equals("STEP_4"))
					{
						strSQL = @"

Declare @now datetime
Set @now = getdate() 
Set @now = Dateadd( day , -3 , @now ) 

Declare @start datetime 
Set @start = Dateadd( day , -95 , @now ) 

Declare @YYYYMMDD_START nvarchar(8) 
Declare @YYYYMMDD_END nvarchar(8) 

Set @YYYYMMDD_START = convert ( nvarchar(8) , @start , 112 )
Set @YYYYMMDD_END = convert ( nvarchar(8) , @now , 112 )


Delete From [zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [SignType] = '--' 

----
/*
--依日期統計
SELECT [cdate],
  isnull([DeliveryOKTotal],'') as '已配達數',
  isnull([Delivery_Digi],'') as '電子簽收',
  isnull([Delivery_Photo],'') as '拍照簽收',
  isnull([Delivery_Paper],'') as '紙本簽收',
  isnull([Delivery_Paper_99],'') as '轉聯運簽收',
  isnull([DeliveryButNoPaper_Paper],'') as '紙本簽收 但是沒有紙本',
  isnull([DeliveryButNoPaper_Paper_99],'') as '轉聯運簽收 但沒有紙本',
  isnull([Delivery_Mix],'') as '重複 (數位簽收同時存在紙本)'
FROM [dbo].[zzzzCheckCalcResult]
Where cdate >= '20220401'
Order by cdate

Select * From [zzzzCheckCalcResult_2]
Order by cdate , [area_arrive_code]

--依日期+站所統計
SELECT [area_arrive_code] ,
  isnull([DeliveryOKTotal],'') as '已配達數',
  isnull([Delivery_Digi],'') as '電子簽收',
  isnull([Delivery_Photo],'') as '拍照簽收',
  isnull([Delivery_Paper],'') as '紙本簽收',
  isnull([Delivery_Paper_99],'') as '轉聯運簽收',
  isnull([DeliveryButNoPaper_Paper],'') as '紙本簽收 但是沒有紙本',
  isnull([DeliveryButNoPaper_Paper_99],'') as '轉聯運簽收 但沒有紙本',
  isnull([Delivery_Mix],'') as '重複 (數位簽收同時存在紙本)'
FROM [dbo].[zzzzCheckCalcResult_2]
Where cdate >= '20220401'
Group by [area_arrive_code]
Order by [area_arrive_code]

SELECT [area_arrive_code] ,
  sum([DeliveryOKTotal]) as '已配達數',
  sum([Delivery_Digi]) as '電子簽收',
  sum([Delivery_Photo]) as '拍照簽收',
  sum([Delivery_Paper]) as '紙本簽收',
  sum([Delivery_Paper_99]) as '轉聯運簽收',
  sum([DeliveryButNoPaper_Paper]) as '紙本簽收 但是沒有紙本',
  sum([DeliveryButNoPaper_Paper_99]) as '轉聯運簽收 但沒有紙本',
  sum([Delivery_Mix]) as '重複 (數位簽收同時存在紙本)'
FROM [dbo].[zzzzCheckCalcResult_2]
Where cdate >= '20220401'
Group by [area_arrive_code]
Order by [area_arrive_code]

SELECT [area_arrive_code] ,
  sum(isnull([DeliveryOKTotal],'0')) as '已配達數'
FROM [dbo].[zzzzCheckCalcResult_2]
Where cdate >= '20220401'
Group by [area_arrive_code]

SELECT sum(isnull([DeliveryOKTotal],'0')) as '已配達數'
FROM [dbo].[zzzzCheckCalcResult_2]
Where cdate >= '20220401'


*/

Delete From zzzzCheckCalcResult
Where cdate >= @YYYYMMDD_START 

Insert Into zzzzCheckCalcResult (cdate , [DeliveryOKTotal])
Select cdate , count(*) From [zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
Group by cdate


Update zzzzCheckCalcResult
	Set Delivery_Digi = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '電子簽名'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate



Update zzzzCheckCalcResult
	Set Delivery_Photo = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '簽單拍照'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate




Update zzzzCheckCalcResult
	Set Delivery_Paper_99 = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code = '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) = '9' or right(area_arrive_code,1) = '9') )
	)
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate



Update zzzzCheckCalcResult
	Set Delivery_Paper = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code <> '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) <> '9' and right(area_arrive_code,1) <> '9') )
		or 
		( area_arrive_code = '' or area_arrive_code not like '[0-9][0-9]' )
	)
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate



Update zzzzCheckCalcResult
	Set DeliveryButNoPaper_Paper = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second <> 'OK' and ImageType <> 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code <> '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) <> '9' and right(area_arrive_code,1) <> '9') )
		or 
		( area_arrive_code = '' or area_arrive_code not like '[0-9][0-9]' )
	)
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate




Update zzzzCheckCalcResult
	Set DeliveryButNoPaper_Paper_99 = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second <> 'OK' and ImageType <> 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code = '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) = '9' or right(area_arrive_code,1) = '9') )
	)
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate


Update zzzzCheckCalcResult
	Set Delivery_Mix = a.c
From 
(
SELECT	[cdate],
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType in ( '電子簽名' , '簽單拍照' )
and [ImageType] = 'OK'
and [ImageType_Second] = 'OK'
Group by [cdate],
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where zzzzCheckCalcResult.cdate = a.cdate



----


Delete From [zzzzCheckCalcResult_2]
Where cdate >= @YYYYMMDD_START 


Insert Into [zzzzCheckCalcResult_2] (cdate , [area_arrive_code] , [DeliveryOKTotal])
Select cdate , isnull([area_arrive_code],'') , count(*) From [zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
Group by cdate , isnull([area_arrive_code],'')


Update [zzzzCheckCalcResult_2]
	Set Delivery_Digi = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '電子簽名'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]



Update [zzzzCheckCalcResult_2]
	Set Delivery_Photo = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '簽單拍照'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]




Update [zzzzCheckCalcResult_2]
	Set Delivery_Paper_99 = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code = '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) = '9' or right(area_arrive_code,1) = '9') )
	)
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]



Update [zzzzCheckCalcResult_2]
	Set Delivery_Paper = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second = 'OK' or ImageType = 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code <> '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) <> '9' and right(area_arrive_code,1) <> '9') )
		or 
		( area_arrive_code = '' or area_arrive_code not like '[0-9][0-9]' )
	)
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]



Update [zzzzCheckCalcResult_2]
	Set DeliveryButNoPaper_Paper = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second <> 'OK' and ImageType <> 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code <> '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) <> '9' and right(area_arrive_code,1) <> '9') )
		or 
		( area_arrive_code = '' or area_arrive_code not like '[0-9][0-9]' )
	)
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]




Update [zzzzCheckCalcResult_2]
	Set DeliveryButNoPaper_Paper_99 = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType = '紙本'
and ( ImageType_Second <> 'OK' and ImageType <> 'OK' )
and (
		( cdate <= '20220516' and area_arrive_code = '99' )
		or 
		( cdate >'20220516' and ( left(area_arrive_code,1) = '9' or right(area_arrive_code,1) = '9') )
	)
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]


Update [zzzzCheckCalcResult_2]
	Set Delivery_Mix = a.c
From 
(
SELECT	[cdate],
		[area_arrive_code] , 
		SignType ,
		[ImageType] , 	
		count(*) as c
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START 
and [DeliveryType] = '已配達'
and SignType in ( '電子簽名' , '簽單拍照' )
and [ImageType] = 'OK'
and [ImageType_Second] = 'OK'
Group by [cdate],
			[area_arrive_code] , 
			[DeliveryType],
			SignType ,
			[ImageType]
) a 
Where [zzzzCheckCalcResult_2].cdate = a.cdate 
and [zzzzCheckCalcResult_2].[area_arrive_code] = a.[area_arrive_code]


";
					}
					cn.Execute(strSQL, commandTimeout: 200);
				}
			}
			catch (Exception e)
			{

			}
		}

		public static List<CheckObj> getCheckList_STEP3()
		{
			List<CheckObj> returnData = new List<CheckObj>();

			try
			{
				using (var cn = new SqlConnection(strConn))
				{
					/*

Select	a.cdate , 
		a.check_number , 
		a.area_arrive_code , 
		DeliveryType , 
		case 
			When a.DeliveryType = '在途' then '--'
			When b.SignType is null then '紙本'
			When b.SignType = '-1' then '不明'
			else b.SignType
		End as SignType ,
		'' as ImageType , 
		c.sign_field_image , 
		c.sign_form_image
		Into zzzzCheck
From 
(
Select	check_number ,
		convert ( nvarchar(8) , cdate , 112 )  as cdate ,
		area_arrive_code ,
		case 
			When latest_scan_item in ('3','4') then '已配達'
			else '在途'
		End as DeliveryType 
From tcDeliveryRequests
Where cdate >= '20220401' and cdate < '20220514'
and ( cancel_date is null or cancel_date = '' )
and ship_date is not null
) a 
left join 
(
Select	CheckNumber as check_number , 
		case 
		   when SignType = '1' then '電子簽名' 
		   when SignType = '2' then 'QRcode簽名'
		   when SignType = '3' then '簽單拍照'
		   else '-1' 
		end SignType  
From SignImage
) b on a.check_number = b.check_number
left join 
(
Select  check_number , sign_field_image , sign_form_image From ttDeliveryScanLog with (nolock)
Where cdate >= '2022-04-01'
and scan_item in ('4')
and log_id in (
				Select  max(log_id) as log_id From ttDeliveryScanLog with (nolock)
				Where cdate >= '2022-04-01'
				and scan_item in ('4')
				and sign_field_image <> '' 
				and sign_form_image <> ''
				Group by check_number
			)
) c on A.check_number = c.check_number
Order by a.cdate
					*/

					string strSQL = @"
Declare @now datetime
Set @now = getdate() 
Set @now = Dateadd( day , -3 , @now ) 

Declare @start datetime 
Set @start = Dateadd( day , -95 , @now ) 

Declare @YYYYMMDD_START nvarchar(8) 
Declare @YYYYMMDD_END nvarchar(8) 

Set @YYYYMMDD_START = convert ( nvarchar(8) , @start , 112 )
Set @YYYYMMDD_END = convert ( nvarchar(8) , @now , 112 )


SELECT	
		[cdate],
		[check_number],
		[DeliveryType],
		[SignType],
		[ImageType] , 
		[ImageType_Second] ,
		isnull(sign_field_image,'') as sign_field_image , 
		isnull(sign_form_image,'') as sign_form_image
FROM [dbo].[zzzzCheck]
Where cdate >= @YYYYMMDD_START
Order by cdate 
";
					returnData = (List<CheckObj>)cn.Query<CheckObj>(strSQL,commandTimeout:300);
				}
			}
			catch (Exception e)
			{

			}

			return returnData;
		}



		public static void updateObj(CheckObj tobj)
		{

			try
			{
				using (var cn = new SqlConnection(strConn))
				{


					string strSQL = @"
    Update [zzzzCheck]
    Set [ImageType] = @ImageType , 
		[ImageType_Second] = @ImageType_Second
    Where check_number = @check_number 
	and cdate = @cdate";
					cn.ExecuteScalar(strSQL, new
					{
						ImageType = tobj.ImageType ,
						ImageType_Second = tobj.ImageType_Second ,
						check_number = tobj.check_number ,
						cdate = tobj.cdate
					});
				}
			}
			catch (Exception e)
			{

			}
		}





	}

}


public class CheckNumberObj
{
	public string request_id = "";
	public string check_number = "";
	public string sign_field_image = "";
	public string sign_form_image = "";
	public int _35Exists = 0;
	public string filePath = "";
	public int _35Exist_seconds = 0;
	public string filePath_second = "";
	public int _S3Exists = 0;
	public string _S3filePath = "";
}


public class AddressSource
{
	public int id;
	public string InitialAddress;
	public DateTime CreateTime;
}


public class CheckObj
{
	public string cdate = "";
	public string check_number = "";
	public string DeliveryType = "";
	public string SignType = "";

	public string ImageType = "";
	public string ImageType_Second = "";

	public string sign_field_image = "";
	public string sign_form_image = "";
}





public class ParseAddress_itri_Model
{
	public ParseAddress_itri_Model()
	{
		CreateTime = DateTime.Now;
		IsArtificial = true;
		Active = true;
		Type = 9;
	}

	public string InitialAddress { get; set; }

	//郵遞區號
	public string ZipCode { get; set; }

	//縣市名
	public string CityName { get; set; }

	//區域名
	public string AreaName { get; set; }

	//路名
	public string RoadName { get; set; }

	//巷號
	public int LaneNo { get; set; }

	//巷號奇偶數
	// 0:偶數 1:奇數 2:沒有巷號
	public int LaneEven { get; set; }

	//弄號
	public int AlleyNo { get; set; }

	//弄號奇偶數
	// 0:偶數 1:奇數 2:沒有弄號
	public int AlleyEven { get; set; }

	//完整的門號, 含非數字
	//如: 萬華區東園街145-1號 > 145-1
	public string AddressOrigin { get; set; }
	//門號
	public int AddressNo { get; set; }
	//奇偶數判斷
	// 0:偶數 1:奇數 2:沒有門號
	public int AddressEven { get; set; }

	//原始註記樓層
	public string FloorOrigin { get; set; }

	//樓層
	public int FloorNo { get; set; }

	//第一個出現的號碼
	public int FstNo { get; set; }

	//第一個出現號碼的奇偶數
	public int FstEven { get; set; }

	//巷之後出現的第一個號碼
	public int SndNo { get; set; }

	//巷之後出現的第一個號碼的奇偶數
	public int SndEven { get; set; }

	public string StationCode { get; set; }

	public string StationName { get; set; }

	public string SalesDriverCode { get; set; }

	public string MotorcycleDriverCode { get; set; }

	public string StackCode { get; set; }

	public DateTime CreateTime { get; set; }

	public bool IsArtificial { get; set; }

	public bool Active { get; set; }

	public int Type { get; set; }

}

