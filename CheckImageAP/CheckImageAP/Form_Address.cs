﻿using CheckImageAP.DB;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckImageAP
{
    public partial class Form_Address : Form
    {
        public Form_Address()
        {
            InitializeComponent();
        }

        private void Form_Address_Load(object sender, EventArgs e)
        {
            yyyymmdd.Text = DateTime.Now.AddDays(-15).ToString("yyyyMMdd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string strStart = "";
            strStart = int.Parse(yyyymmdd.Text).ToString().Trim();
            if (strStart.Equals(""))
            {
                yyyymmdd.Text = DateTime.Now.AddDays(-15).ToString("yyyyMMdd");
                strStart = int.Parse(yyyymmdd.Text).ToString().Trim();
            }
            List<AddressSource> returnData = new List<AddressSource>();
            returnData = DBClass.getAddressSourceList(strStart);

            txtBox.AppendText("START  \r\n ");
            txtBox.AppendText(DateTime.Now.ToString("yyyy/MM/dd HH:mm") + " \r\n ");
            txtBox.AppendText("共 " + returnData.Count.ToString() + "筆 \r\n ");

            System.Threading.Thread.Sleep(1);
            Application.DoEvents();

            var AddressParsingURL_1 = "http://map2.fs-express.com.tw/Address_itri/GetAddressRsult";
            var AddressParsingURL_2 = "http://map2.fs-express.com.tw/Address_itri/GetAddressRsult_Map8_2";

            string[] urlArr = { "http://map2.fs-express.com.tw/Address_itri/GetAddressRsult", 
                            "http://map2.fs-express.com.tw/Address_itri/GetAddressRsult_Map8_2" };
            for ( int i = 0; i < returnData.Count; i++)
            {
                //打API
                for ( int j = 0; j < 2; j++)
                {
                    var client = new RestClient(urlArr[j]);
                    var request = new RestRequest(Method.POST);
                    var isSave = true;
                    var Type = "1";
                    var ComeFrom = "777";
                    var CustomerCode = "1";

                    request.RequestFormat = DataFormat.Json;
                    request.AddJsonBody(new { isSave , Address = returnData[i].InitialAddress.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

                    var response = client.Post<ParseAddress_itri_Model>(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        if ( i % 200 == 0)
                        {
                            txtBox.AppendText("正在執行檢查 第" + i.ToString() + "筆 \r\n ");
                            System.Threading.Thread.Sleep(1);
                            Application.DoEvents();
                        }
                    }
                }


            }
        }
    }
}
