﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace DB_BackFileToNas
{
    public class DbClass
    {
        public static void InsertRecord(JobRecord tobj)
        {
            string strConn = System.Configuration.ConfigurationManager.AppSettings["connstr_log"].ToString();
            try
            {
                using (var cn = new SqlConnection(strConn))
                {


                    string strSQL = @"
INSERT INTO [dbo].[JobRecord]
           ([Type],[SubType],[Action],[Subject],[ContentBody],[LinkRelationData],[IsDoNotification],[MEMO])
     VALUES
           (@Type , @SubType , @Action , @Subject , @ContentBody , @LinkRelationData , @IsDoNotification , @MEMO  ) ";
                    cn.ExecuteScalar(strSQL, new
                    {
                        Type = tobj.Type,
                        SubType = tobj.SubType,
                        Action = tobj.Action,
                        Subject = tobj.Subject,
                        ContentBody = tobj.ContentBody,
                        LinkRelationData = tobj.LinkRelationData,
                        IsDoNotification = tobj.IsDoNotification,
                        MEMO = tobj.MEMO
                    });
                }
            }
            catch (Exception e)
            {

            }
        }

        public static void InsertNotification(Notification tobj)
        {
            string strConn = System.Configuration.ConfigurationManager.AppSettings["connstr_log"].ToString();
            try
            {
                using (var cn = new SqlConnection(strConn))
                {
                    string strSQL = @"
INSERT INTO [dbo].[Notification]
           ([Type],[Recipient],[Subject],[Body],[CreateTime],[CreateUser])
     VALUES
           (@Type,@Recipient,@Subject,@Body,@CreateTime,@CreateUser)";
                    cn.ExecuteScalar(strSQL, new
                    {
                        Type = tobj.Type,
                        Recipient = tobj.Recipient,
                        Subject = tobj.Subject,
                        Body = tobj.Body,
                        CreateTime = tobj.CreateTime,
                        CreateUser = tobj.CreateUser
                    });
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    public class JobRecord
    {
        public JobRecord(string strType, string strSubType, string strAction, string strSubject, string strContentBody,
                           string strLinkRelationData = "" , int iIsDoNotification = 0 , string strMEMO = "")
        {
            Type = strType;
            SubType = strSubType;
            Action = strAction;
            Subject = strSubject;
            ContentBody = strContentBody;
            LinkRelationData = strLinkRelationData;
            IsDoNotification = iIsDoNotification;
            MEMO = strMEMO;
        }
        public string Type = "";
        public string SubType = "";
        public string Action = "";
        public string Subject = "";
        public string ContentBody = "";
        public string LinkRelationData = "";
        public int IsDoNotification = 0;
        public string MEMO = "";
    }


    public class Notification
    {
        public Notification(string strSubject, string strBody , string strCreateUser)
        {
            Subject = strSubject;
            Body = strBody;
            CreateTime = DateTime.Now;
            CreateUser = strCreateUser;
        }
        public string Type = "1";
        public string Recipient = "Bearer HYxj7V2YZU0LZxEbLTa3USyuSmHM8yZw5wBS5StIxTh";
        public string Subject = "";
        public string Body = "";
        public DateTime CreateTime ;
        public string CreateUser = "";
    }
}
