﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_BackFileToNas
{
    public class LogFile
    {
        public static string strLogDirPath = "LOG"; 

        public static string checkAndRetuenLogPath()
        {
            string strYYYYMMDD = DateTime.Now.ToString("yyyyMMdd") + ".txt"; 

            if (System.IO.Directory.Exists(strLogDirPath) == false)
            {
                System.IO.Directory.CreateDirectory(strLogDirPath);
            }

            if (System.IO.Directory.Exists(strLogDirPath) == false)
            {
                System.IO.File.CreateText(strLogDirPath + @"\" + strYYYYMMDD);
            }

            return strLogDirPath + @"\" + strYYYYMMDD;

        }

        public static void  writeLog(string strMsg)
        {
            string strPath = checkAndRetuenLogPath();

            System.IO.File.AppendAllText(strPath , "---------------" + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, strMsg + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, "---------------" + ControlChars.NewLine);
        }

        public static void writeLog(Exception ex)
        {
            string strPath = checkAndRetuenLogPath();

            System.IO.File.AppendAllText(strPath, "---------------" + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, ex.StackTrace + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, ex.Source + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, ex.Message + ControlChars.NewLine);
            System.IO.File.AppendAllText(strPath, "---------------" + ControlChars.NewLine);
        }

    }
}
