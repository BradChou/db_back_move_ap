﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_BackFileToNas
{
    class Program
    {
        /// <summary>
        /// DB備份的資料夾
        /// </summary>
        public static string strDbBackWritePath = "";

        /// <summary>
        /// 資料庫備份的資料夾數量
        /// </summary>
        public static List<string> listDbFolderPath = new List<string>();

        /// <summary>
        /// 遠端NAS的路徑
        /// </summary>
        public static string strNasWritePath = "";

        /// <summary>
        /// 備份保留天數
        /// </summary>
        public static int iBackupDays = -10;

        /// <summary>
        /// AP執行發動的時間
        /// </summary>
        public static DateTime nowDT = DateTime.Now;


        /// <summary>
        /// 判斷要搬移備份哪一個DB / 文中備份
        /// </summary>
        public static string isMAIN_104_FIN = "";

        /// <summary>
        /// 取得基本設定資料
        /// </summary>
        private static void baseDataGet()
        {

            try
            {
                isMAIN_104_FIN = System.Configuration.ConfigurationManager.AppSettings["MAIN_104_FIN"].ToString();
                isMAIN_104_FIN = isMAIN_104_FIN.ToUpper();

                if (isMAIN_104_FIN != "MAIN" && isMAIN_104_FIN != "104" && isMAIN_104_FIN != "FIN" && isMAIN_104_FIN != "NEW" )
                {
                    isMAIN_104_FIN = "MAIN";
                }
            }
            catch(Exception ex)
            {
                isMAIN_104_FIN = "MAIN";
            }

            try
            {
                switch (isMAIN_104_FIN)
                {
                    case "MAIN":
                        strType = "10000";
                        strSubType = "001";
                        strTypeName = "AP排程";
                        strSubTypeName = "DB備份排程";

                        strDbBackWritePath = System.Configuration.ConfigurationManager.AppSettings["strDbBackWritePath"].ToString();

                        listDbFolderPath = System.IO.Directory.GetDirectories(strDbBackWritePath).ToList<string>();

                        strNasWritePath = System.Configuration.ConfigurationManager.AppSettings["strNasWritePath"].ToString();

                        break;
                    case "104":
                        strType = "10000";
                        strSubType = "003";
                        strTypeName = "AP排程";
                        strSubTypeName = "104備份排程";

                        strDbBackWritePath = System.Configuration.ConfigurationManager.AppSettings["strDbBackWritePath_104"].ToString();

                        listDbFolderPath = System.IO.Directory.GetDirectories(strDbBackWritePath).ToList<string>();

                        strNasWritePath = System.Configuration.ConfigurationManager.AppSettings["strNasWritePath_104"].ToString();
                        break;
                    case "FIN":
                        strType = "10000";
                        strSubType = "002";
                        strTypeName = "AP排程";
                        strSubTypeName = "文中ERP備份排程";

                        strDbBackWritePath = System.Configuration.ConfigurationManager.AppSettings["strDbBackWritePath_FIN"].ToString();

                        listDbFolderPath = System.IO.Directory.GetDirectories(strDbBackWritePath).ToList<string>();

                        strNasWritePath = System.Configuration.ConfigurationManager.AppSettings["strNasWritePath_FIN"].ToString();
                        break;
                    case "NEW":
                        strType = "10000";
                        strSubType = "004";
                        strTypeName = "AP排程";
                        strSubTypeName = "新系統備份排程";

                        strDbBackWritePath = System.Configuration.ConfigurationManager.AppSettings["strDbBackWritePath_NEW"].ToString();

                        listDbFolderPath = System.IO.Directory.GetDirectories(strDbBackWritePath).ToList<string>();

                        strNasWritePath = System.Configuration.ConfigurationManager.AppSettings["strNasWritePath_NEW"].ToString();
                        break;
                }

            }
            catch(Exception ex)
            {

            }

            try
            {
                iBackupDays = int.Parse( System.Configuration.ConfigurationManager.AppSettings["backupDays"].ToString() );
                iBackupDays = 0 - iBackupDays;
            }
            catch(Exception ex)
            {
                iBackupDays = -10;
            }
        }

        /// <summary>
        /// 逐一判斷備份檔案
        /// 1. 是否需要複製到遠端 (今天)
        /// 2. 是否要刪除 (10天前)
        /// </summary>
        /// <param name="strDirectFull">完整路徑</param>
        private static void getBackFileAndSaveToNas(string strDirectFull)
        {
            //目錄名稱 同時也是資料庫名稱
            string strDirName = new System.IO.DirectoryInfo(strDirectFull).Name;

            List<string> listTotalBackFile = new List<string>();

            listTotalBackFile = System.IO.Directory.GetFiles(strDirectFull).ToList<string>();

            foreach( string strSinglePath in listTotalBackFile)
            {
                DateTime thisDt = System.IO.File.GetCreationTime(strSinglePath);

                //檔名
                string strFileName = new System.IO.FileInfo(strSinglePath).Name;

                //要檢查多久時間以內產生的檔案

                //主DB系列
                //如果是早上6點 ~ 早上8點
                //要檢查最近6小時 ( 早上6點進行完整備份 )
                //其餘時段 ( 9點 ~ 22點 ) 
                //檢查1小時內 ( 9點/12點/15點/18點/21點 會進行差異備份 ) 
                int iCheckKey = -6; 
                if (nowDT.Hour >= 9 && nowDT.Hour <= 22)
                {
                    iCheckKey = -1;
                }

                //其餘DB
                //一天只進行一次 早上6點進行完整備份
                if (isMAIN_104_FIN == "NEW")
                {
                    iCheckKey = -12;
                }

                //今天產生的備份 , 就複製去遠端NAS
                if (
                    ( thisDt >= nowDT.AddHours(iCheckKey) && new System.IO.FileInfo(strSinglePath).Extension.ToLower()== ".bak")
                    ||
                    (thisDt >= nowDT.AddHours(iCheckKey) && new System.IO.FileInfo(strSinglePath).Extension.ToLower() == ".trn")
                   )
                {
                    

                    if ( System.IO.Directory.Exists(strNasWritePath + strDirName) == false)
                    {
                        System.IO.Directory.CreateDirectory(strNasWritePath + strDirName);
                    }
                    LogFile.writeLog("備份中......--" + strFileName);

                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step3", "備份中......--" + strFileName, "", "", 0, "")
                            );

                    System.IO.File.Copy(strSinglePath, strNasWritePath + strDirName + @"\" + strFileName , true);
                }

                if (thisDt < nowDT.AddDays(iBackupDays) )
                {
                    LogFile.writeLog("準備刪除舊檔......--" + strSinglePath);

                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step4", "準備刪除舊檔......--" + strSinglePath, "", "", 0, "")
                            );

                    System.IO.File.Delete(strSinglePath);
                }
            }

        }


        private static void getBackFileAndSaveToNas_FIN(string strDirectFull)
        {
            //目錄名稱 同時也是文中服務備份清單
            string strDirName = new System.IO.DirectoryInfo(strDirectFull).Name;

            DateTime thisDt = System.IO.Directory.GetCreationTime(strDirectFull);

            if (thisDt >= nowDT.AddHours(-18) )
            {
                LogFile.writeLog("備份中......--" + strDirName);
                DirectoryCopy(strDirectFull, strNasWritePath + strDirName, true);
            }
           
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                LogFile.writeLog("Source directory does not exist or could not be found......--" + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }

        public static string strType = "";
        public static string strSubType = "";

        public static string strTypeName = "";
        public static string strSubTypeName = "";

        public static DateTime timeStart  ;
        public static DateTime timeEnd;

        static void Main(string[] args)
        {
            timeStart = DateTime.Now;
            string strContent = "";
            try
            {


                baseDataGet();

                LogFile.writeLog("準備開始DB備份");

                DbClass.InsertRecord(
                        new JobRecord(strType , strSubType , "step1-Start" , "準備開始DB備份" , "" , "" , 0 , "")
                    );

                if (strDbBackWritePath == "" || strNasWritePath == "" || listDbFolderPath.Count == 0)
                {
                    LogFile.writeLog("基本設定檔錯誤 或是 沒有需要備份的DB資料夾");

                    DbClass.InsertRecord(
                            new JobRecord(strType, strSubType, "step-Error", "基本設定檔錯誤 或是 沒有需要備份的DB資料夾", "", "", 0, "")
                        );

                    timeEnd = DateTime.Now;
                    strContent = "開始時間 " + timeStart.ToString("yyyy-MM-dd HH:mm:ss") + " / 結束時間 " + timeEnd.ToString("yyyy-MM-dd HH:mm:ss");

                    DbClass.InsertRecord(
                            new JobRecord(strType, strSubType, "step-Final", strTypeName + " - " + strSubTypeName + "--基本設定檔錯誤 或是 沒有需要備份的DB資料夾", strContent, "", 0, strTypeName + " - " + strSubTypeName)
                        );

                    return;
                }


                if (isMAIN_104_FIN.Equals("MAIN") || isMAIN_104_FIN.Equals("104") || isMAIN_104_FIN.Equals("NEW"))
                {
                    //兩層目錄
                    foreach (string strFolderPath in listDbFolderPath)
                    {
                        LogFile.writeLog("正在準備備份-- (" + isMAIN_104_FIN + ") " + strFolderPath);

                        DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step2", "正在準備備份-- (" + isMAIN_104_FIN + ") ", "", strFolderPath, 0, "")
                            );

                        getBackFileAndSaveToNas(strFolderPath);
                    }

                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step4-End", "準備結束DB備份", "", "", 0, "")
                            );

                    timeEnd = DateTime.Now;
                    strContent = "開始時間 " + timeStart.ToString("yyyy-MM-dd HH:mm:ss") + " / 結束時間 " + timeEnd.ToString("yyyy-MM-dd HH:mm:ss");
                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step-Final", strTypeName + " - " + strSubTypeName + "--執行成功", strContent, "", 0, strTypeName + " - " + strSubTypeName)
                            );


                }
                else if (isMAIN_104_FIN.Equals("FIN"))
                {
                    //多層次目錄+檔案
                    //而且每天備份目錄是獨立的
                    //乾脆直接遞迴COPY目錄        

                    LogFile.writeLog("正在準備備份-- (" + isMAIN_104_FIN + ") " );

                    DbClass.InsertRecord(
                            new JobRecord(strType, strSubType, "step2", "正在準備備份-- (" + isMAIN_104_FIN + ") ", "", "" , 0, "")
                        );

                    foreach (string strFolderPath in listDbFolderPath)
                    {
                        //LogFile.writeLog("正在準備備份-- (" + isMAIN_104_FIN + ") " + strFolderPath);
                        getBackFileAndSaveToNas_FIN(strFolderPath);
                    }

                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step4-End", "準備結束DB備份", "", "", 0, "")
                            );

                    timeEnd = DateTime.Now;
                    strContent = "開始時間 " + timeStart.ToString("yyyy-MM-dd HH:mm:ss") + " / 結束時間 " + timeEnd.ToString("yyyy-MM-dd HH:mm:ss");
                    DbClass.InsertRecord(
                                new JobRecord(strType, strSubType, "step-Final", strTypeName + " - " + strSubTypeName + "--執行成功", strContent, "", 0, strTypeName + " - " + strSubTypeName)
                            );


                }




            }
            catch(Exception ex)
            {
                LogFile.writeLog(ex);


                DbClass.InsertRecord(
                        new JobRecord(strType, strSubType, "step-Error", ex.Message, "", "", 0, "")
                    );

                timeEnd = DateTime.Now;
                strContent = "開始時間 " + timeStart.ToString("yyyy-MM-dd HH:mm:ss") + " / 結束時間 " + timeEnd.ToString("yyyy-MM-dd HH:mm:ss");

                DbClass.InsertRecord(
                        new JobRecord(strType, strSubType, "step-Final", strTypeName + " - " + strSubTypeName + "--" + ex.Message , strContent, "", 1, strTypeName + " - " + strSubTypeName)
                    );

                return;

            }

            LogFile.writeLog("準備結束DB備份");

            return;

        }
    }
}
